This is the readme file for all users and all developers to understand what we have done in this project.

The target of this project is to estbalished a mathatical method which can predict an 3D instantaneous vector and scalar field based on a series of surface measurement. The process of prediction is established on the adjoint method and optimization algalgorithm. In combustion area, the vector we wish to predict is the velocity field and the scalar is the G-value which represents the distance to the flame surface. The whole part consists of two main parts, velocity prediction and G-value prediction.

To realize this work step by step, the velocity prediction is seperated into three steps, 1D, 2D and 3D, and the G-value prediction is seperated into two steps, 2D and 3D.

The core of this project is to calculate the gradient of the error function by applying the adjoint method which consider the initial velocity and boundary conditions as the optimization factors. Through optimize the initial velocity, we can have a prediction of the initial velocity distribution which is an instantaneuous characterisistic. 

Next is the journal of the general code.(Every developer, if you have any new update about the general thing which is necessary to be described, please leave it here)
11/17/2020	Initial upload file from desktop.
11/18/2020	update journals and readme files.




Notes:
1. Write down your journal of whenever you push an update to the online repository.
2. When you come across a conflict between online repository and you local repository, which is caused by different version, please do not use the force operation to replace the online repository. You should either pull from online and merge with you local repository, or add a new branch and ask the manager for help.
3. Each developer is free to use either matlab or python, just put them in the right folder. There are two journals and one readme files in this project. One journal for matlab for matlab code, one for python and the readme is for general modification.

Instruction for gitlab(just use my account as an example)

Git global setup

git config --global user.name "Y. Zheng"
git config --global user.email "yz615@cam.ac.uk"

Create a new repository

git clone git@gitlab.developers.cam.ac.uk:yz615/turbulent_combustion_quasi_instantaneous_3d_measurement.git
cd turbulent_combustion_quasi_instantaneous_3d_measurement
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master


Push an existing folder

cd existing_folder
git init
git remote add origin git@gitlab.developers.cam.ac.uk:yz615/turbulent_combustion_quasi_instantaneous_3d_measurement.git
git add .
git commit -m "Initial commit"
git push -u origin master


Push an existing Git repository

cd existing_repo
git remote rename origin old-origin
git remote add origin git@gitlab.developers.cam.ac.uk:yz615/turbulent_combustion_quasi_instantaneous_3d_measurement.git
git push -u origin --all
git push -u origin --tags
