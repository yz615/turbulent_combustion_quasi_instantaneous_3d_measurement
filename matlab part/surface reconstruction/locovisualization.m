function y=locovisualization(p1,p2,p3,p4,factor,mode2)

if mode2==4
    x=zeros(3,3);
    y=zeros(3,3);
    h=zeros(3,3);
    x(1,1)=p1(1);
    x(3,3)=p4(1);
    y(1,1)=p1(2);
    y(3,3)=p4(2);
    for i=1:1:3
        for j=1:1:3
            h(i,j)=factor(1)*x(i,j)*y(i,j)+factor(2)*x(i,j)+factor(3)*y(i,j)+factor(4);
        end
    end
    surf(x,y,h)
    hold on
end