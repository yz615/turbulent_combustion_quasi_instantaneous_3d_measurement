%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%             FUNCTION CODE                            %
%              importing.M                             %
%     Created by Yutao Zheng                           %
%   This function is to import data from excel files   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [inputdata,a,b]=importing(filename,sheetmarker,num_sli)
%this function is to import data into an acceptable format
width=zeros(num_sli,1);
length=zeros(num_sli,1);
%measure the size
for i=1:1:num_sli
    sheet=[sheetmarker num2str(i)];
    A=xlsread(filename,sheet);
    sizeA=size(A);
    length(i)=sizeA(1);
    width(i)=sizeA(2)/2;
end
a=width;%identify how much lines we have,0 
b=length;
max_length=max(length);
max_width=max(width);

if max(width)==1
    inputdata=zeros(max_length,2,num_sli);
    for i=1:1:num_sli
        sheet=[sheetmarker num2str(i)];
        inputdata(1:length(i),:,i)=xlsread(filename,sheet);
    end    
else
    inputdata=zeros(max_length,2,num_sli,max_width);
    for i=1:1:num_sli
        sheet=[sheetmarker num2str(i)];
        temp=xlsread(filename,sheet);
        for j=1:1:width(i)
            inputdata(1:length(i),:,i,2*j:2*j+1)=temp(1:length(i),2*j:2*j+1);
        end
    end
end