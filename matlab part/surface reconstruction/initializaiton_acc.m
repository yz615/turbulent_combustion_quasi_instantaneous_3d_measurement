%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%             FUNCTION CODE                            %
%               initialization.M                       %
%     Created by Yutao Zheng                           %
%   This function is to create a series of slicing and %
%   export them into a excel file
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function y=initializaiton_acc(h,r,pixel,num_sli,edge,p0,filename,sheetmarker)

    frames=zeros(pixel,2,num_sli);%each one is the slice_H;
    listpn=zeros(num_sli,2);
    filecreate(filename,sheetmarker,num_sli);
    for i=1:1:num_sli
        listpn(i,2)=1.2*r*(2*(i-num_sli)/num_sli+1);
        pn=listpn(i,:);
        gap_x=(edge(2)-edge(1))/pixel;
        x=edge(1)+gap_x/2:gap_x:edge(2)-gap_x/2;
        %gap_y=gap_x*(pn(2)-p0(2))/(pn(1)-p0(1));
        if pn(1)-p0(1)==0
            y=p0(2)*ones(1,pixel);
        else
            y=p0(2)*ones(1,pixel)+(pn(2)-p0(2))*(x-p0(1)*ones(1,pixel))/(pn(1)-p0(1));
        end
        h_out=zeros(pixel,1);
        for j=1:1:pixel
            if x(j)^2+y(j)^2<r^2
                h_out(j)=h*(r-sqrt(x(j)^2+y(j)^2))/r;
            end
        end
        pixel_num=1:1:pixel;
        frames(:,:,i)=[pixel_num' h_out];
        sheet=[sheetmarker num2str(i)];
        xlswrite(filename,frames(:,:,i),sheet)
    end
    y=frames;
end

function filecreate(filename,sheetmarker,num_sli)
    xlswrite(filename,100) % # create test file
    e = actxserver('Excel.Application'); % # open Activex server
    ewb = e.Workbooks.Open([cd '\' filename]); % # open file (enter full path!)
    for i=1:1:num_sli-1
        ewb.Worksheets.Add([],ewb.Worksheets.Item(ewb.Worksheets.Count));
    end
    for i=1:1:num_sli
        sheetname=[sheetmarker num2str(i)];
        ewb.Worksheets.Item(i).Name = sheetname; % # rename 1st sheet
    end
    ewb.Save % # save to the same file
    ewb.Close(false)
    e.Quit
    
end
