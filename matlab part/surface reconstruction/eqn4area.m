%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%             FUNCTION CODE                            %
%               eqn4area.M                             %
%     Created by Yutao Zheng                           %
%   This function convert solve the area among         %
%   four points with certain function 4 order function %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function area=eqn4area(factor,p1,p2,p3,p4,mode,r)
%mode: level of accuarcy
%mode=1 low
%mode=2 middle
%mode=3 high(remain unknown)

switch mode
    case 1
        %p_mid=(p1+p2+p3+p4)/4;
            %if mode2==0
        %phps=factor(1)*0.5+factor(2);
        %phpt=factor(1)*0.5+factor(3);
        %limit=10^-1;
        %if abs(p3(1)-p1(1)+p4(1)-p2(1))<limit
        %    pspx=0;%10^3*sign(p3(1)-p1(1)+p4(1)-p2(1));
        %else
        %    pspx=(1/(p3(1)-p1(1)+p4(1)-p2(1)))*2;
        %end
        %if abs(p3(2)-p1(2)+p4(2)-p2(2))<limit
        %    pspy=0;%10^3*sign(p3(2)-p1(2)+p4(2)-p2(2));
        %else
        %    pspy=(1/(p3(2)-p1(2)+p4(2)-p2(2)))*2;
        %end
        %if abs(p2(1)-p1(1)+p4(1)-p3(1))<limit
        %    ptpx=0;%10^3*sign(p2(1)-p1(1)+p4(1)-p3(1));
        %else
        %    ptpx=(1/(p2(1)-p1(1)+p4(1)-p3(1)))*2;
        %end
        %if abs(p2(2)-p1(2)+p4(2)-p3(2))<limit
        %    ptpy=0;%10^3*sign(p2(2)-p1(2)+p4(2)-p3(2));
        %else
        %    ptpy=(1/(p2(2)-p1(2)+p4(2)-p3(2)))*2;
        %end
        %phpx=phps*pspx+phpt*ptpx;
        %phpy=phps*pspy+phpt*ptpy;
        %else
            %if p_mid(1)^2+p_mid(2)^2<r^2
            %    phpx=factor(1)*p_mid(2)+factor(2)-mode2*p_mid(1)/sqrt(p_mid(1)^2+p_mid(2)^2);
            %    phpy=factor(1)*p_mid(1)+factor(3)-mode2*p_mid(2)/sqrt(p_mid(1)^2+p_mid(2)^2);
            %else
            %    phpx=factor(1)*p_mid(2)+factor(2);
            %    phpy=factor(1)*p_mid(1)+factor(3);
            %end
            
        %end
        %dadxdy=sqrt(1+phps^2+phpt^2);
        %area_xy=triarea(p1(1:2),p2(1:2),p3(1:2))+triarea(p4(1:2),p2(1:2),p3(1:2));
        %area=dadxdy*area_xy*limit4points(p1,p2,p3,p4,r);
        area=(triarea(p1,p2,p3)+triarea(p2,p3,p4)+triarea(p1,p2,p4)+triarea(p1,p3,p4))/2;
        area=area*limit4points(p1,p2,p3,p4,r);
    case 2
        N=3;
        cord=zeros(N+1,N+1,3);
        cord(1,1,:)=p1(1:3);
        cord(1,N+1,:)=p2(1:3);
        cord(N+1,1,:)=p3(1:3);
        cord(N+1,N+1,:)=p4(1:3);
        for j=2:1:N
            cord(1,j,:)=p1(1:3)*(1-(j-1)/N)+p2(1:3)*(j-1)/N;
            cord(N+1,j,:)=p3(1:3)*(1-(j-1)/N)+p4(1:3)*(j-1)/N;
        end
        for j=1:1:N+1
            for i=2:1:N
                cord(i,j,:)=cord(1,j,1:3)*(1-(i-1)/N)+cord(N+1,j,1:3)*(i-1)/N;
            end
        end
        area=0;
        for i=1:1:N
            for j=1:1:N
                area=area+eqn4area(factor,cord(i,j,:),cord(i,j+1,:),cord(i+1,j,:),cord(i+1,j+1,:),1,r);
            end
        end
    case 3
        area=0;
end