%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%        convertmesh.M FUNCTION CODE                   %
%     Created by Yutao Zheng                           %
%   This function convert a series of slicing data     %
%   to a well organized mesh group,structured          %
%   or unstructrued                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%% Function Header Cell
% Establishes the passing parameters

%two input
%group_Hn : a group of height information inputed as N*pixel*3
%          N: number of slices; 3: (x,y,h)
%mode : mode of structure mesh(0) or unstructure mesh(1)

%one output
%for structured mesh: nothing
%for unstructured mesh: something

function output=convertmesh(group_Hn,mode)
if mode==0
    % for current version, it is just to switch two lines
    x=group_Hn(:,1,:);
    y=group_Hn(:,2,:);
    h=group_Hn(:,3,:);
    sizeout=size(x);
    output=zeros(sizeout(1),sizeout(3),3);
    output(:,:,1)=x;
    output(:,:,2)=y;
    output(:,:,3)=h;
end


