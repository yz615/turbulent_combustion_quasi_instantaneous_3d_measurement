pixel=[20 30 40 50 60 70 80 90];
error1=[2.52 1.78 1.36 1.10 0.87 0.75 0.71 0.67];
error3=[2.58 1.93 1.30 1.10 0.79 0.72 0.65 0.57];

plot(pixel,error1,'linewidth',2)
hold on 
plot(pixel,error3,'linewidth',2)
xlabel('Number of slices')
ylabel('Error(%)')
legend('Test model 1','Test model 3')
axis([20 90 0 3])
set(gca,'Fontsize',14,'Fontname','Times')
