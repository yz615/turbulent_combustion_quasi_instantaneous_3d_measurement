clc
clear
pi=3.1415926;
%main function for the whole process

%factor initialization
h=24;%20 mm
r=7;%7 mm
e=6;%1 mm

%resolution factor
pixel=160;%scaled pixel
num_sli=31;%number of slices
f_switch=1;%o for off,1 for on;to set the line number inconsistency methods
mode_method=ones(num_sli-1,1);% to handle with line number inconsistency
mode=0;%unstructure mesh

%location of each slicing
edge=[-10 10];
res_p=(edge(2)-edge(1))/pixel;%pixel resolution
p0=[100 0];%(y,x)
listpn=zeros(num_sli,2);
for i=1:1:num_sli
    listpn(i,2)=1.2*r*(2*(i-num_sli)/num_sli+1);
end

stage=1;%(no need for stage statement in further program!)
onoff=1;%small surf visualization;1 for on,0 for off

%importing files
filename='input.xlsx';
sheetmarker='slice_';

%initialization
[frames,pairs,length]=initialization(h,r,e,stage,p0,listpn,edge,num_sli,pixel);
%frames=initializaiton_acc(h,r,pixel,num_sli,edge,p0,filename,sheetmarker);

%[frames,pairs,length]=importing(filename,sheetmarker,num_sli);
figure(2)
for i=1:2:num_sli
   plot(frames(:,1,i),frames(:,2,i),'linewidth',2)
   hold on
end
xlabel('Number of the pixel')
ylabel('h(mm)')
max_length=max(length);
max_pair=max(pairs);
%axis([-0.01 0.01 -0.01 0.01 -0.01 0.03])
%importing
%none

%digitialization
%we have varaibles 'frames' as the input here
if max_pair==1
    line_H=zeros(max_length,3,num_sli);
    for i=1:1:num_sli
        line_H(:,:,i)=digitialization(frames(:,:,i),p0,listpn(i,:),edge,pixel);
    end
    figure(3)
    for i=1:1:num_sli
        line_H_t=[line_H(:,1,i) line_H(:,2,i) line_H(:,3,i)];
        line_H_t(all(line_H_t==0,2),:)=[];
        plot3(line_H_t(:,1),line_H_t(:,2),line_H_t(:,3))
        xlabel('x')
        ylabel('y')
        zlabel('h')
        hold on
    end
    axis([-10 10 -10 10 -10 30])
    %meshing
    mode=0;
    mesh_Hn=convertmesh(line_H,mode);
    figure(5)
    for i=1:1:num_sli
        mesh_Hn_t=[mesh_Hn(:,i,1) mesh_Hn(:,i,2) mesh_Hn(:,i,3)];
        mesh_Hn_t(all(mesh_Hn_t==0,2),:)=[];
        plot3(mesh_Hn_t(:,1),mesh_Hn_t(:,2),mesh_Hn_t(:,3))
        xlabel('x')
        ylabel('y')
        zlabel('h')
        hold on
    end
    axis([-10 10 -10 10 -10 30])
    
    %surfacing
    if onoff==1
        figure(6)
        axis equal
        axis([-10 10 -10 10 -10 30])
    end
    sec=max_length*2;
    surface_f=zeros(sec-1,num_sli-1,4);
    area_total=0;
    for j=1:1:num_sli-1
        [f_temp,area]=surfacing(mesh_Hn(:,j:j+1,:),0,4,onoff,r,sec,j,f_switch,mode_method);
        %surface_f(:,j,:)=f_temp;
        area_total=area_total+area;
    end
    disp(area_total)
    %area calculation
    (area_total-pi*r*sqrt(r^2+h^2))/(pi*r*sqrt(r^2+h^2))
else
    line_H=zeros(max_length,3,num_sli,max_pair);
    for j=1:1:max_pair
        figure(3)
        for i=1:1:num_sli
            if j==1
                tempframes=frames(:,:,i,j);
            else
                tempframes=frames(:,:,i,j);
                tempframes(all(tempframes==0,2),:)=[];
            end
            temp_lineH=digitialization(tempframes,p0,listpn(i,:),edge,pixel);
            [temp_slineH1,temp_slineH2]=size(temp_lineH);
            line_H(1:temp_slineH1,1:temp_slineH2,i,j)=temp_lineH;
            plot3(line_H(1:temp_slineH1,1,i,j),line_H(1:temp_slineH1,2,i,j),line_H(1:temp_slineH1,3,i,j))
            xlabel('x')
            ylabel('y')
            zlabel('h')
            hold on
            axis equal
        end
    end
    %meshing
    mesh_Hn=zeros(pixel,num_sli,3,max_pair);
    figure(5)
    for k=1:1:max_pair
        mesh_Hn(:,:,:,k)=convertmesh(line_H(:,:,:,k),mode);
         for i=1:1:num_sli
             temp_mesh_Hn=[mesh_Hn(:,i,1,k) mesh_Hn(:,i,2,k) mesh_Hn(:,i,3,k)];
             temp_mesh_Hn(all(temp_mesh_Hn==0,2),:)=[];
             plot3(temp_mesh_Hn(:,1),temp_mesh_Hn(:,2),temp_mesh_Hn(:,3))
             xlabel('x')
             ylabel('y')
             zlabel('h')
             hold on
             axis equal
        end
    end
    %surfacing
    %something wrong with it process.
    if onoff==1
        figure(6)
        axis equal
        axis([-10 10 -10 10 -10 30])
    end
    sec=max_length*2;
    surface_f=zeros(sec-1,num_sli-1,max_pair,4);
    area_total=0;
    for j=1:1:num_sli-1
        if max(mode_method)==1
           if max(pairs(j:j+1))==1
               [f_temp,area_temp]=surfacing(mesh_Hn(:,j:j+1,:,1),0,4,onoff,r,sec,[j k],f_switch,pairs(j:j+1),mode_method(j));
               area_total=area_total+area_temp;
           else
               [f_temp,area_temp]=surfacing(mesh_Hn(:,j:j+1,:,:),0,4,onoff,r,sec,[j k],f_switch,pairs(j:j+1),mode_method(j));
               area_total=area_total+area_temp;

            end
        end
    end
    disp(area_total)
    %area calculation
    disp(4*pi*(h*0.1)^2+pi*r*sqrt(r^2+h^2))
    disp(abs(area_total-4*pi*(h*0.1)^2-pi*r*sqrt(r^2+h^2))/(4*pi*(h*0.1)^2+pi*r*sqrt(r^2+h^2)))
end
