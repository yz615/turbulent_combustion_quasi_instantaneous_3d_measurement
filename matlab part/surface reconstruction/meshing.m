%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%             FUNCTION CODE                            %
%               meshing.M                              %
%     Created by Yutao Zheng                           %
%   This function create the meshing data              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function mesh_Hn= meshing(group_Hn,mode)
%UNTITLED7 Summary of this function goes here
%   Detailed explanation goes here

%two input
%group_Hn: a group of height information inputed as N*pixel*3
%N: number of slices
%3: (x,y,h)
%mode: mode of structure mesh(0) or unstructure mesh(1)

%one output
%for structured mesh: nothing
%for unstructured mesh: something
if mode==0
    mesh_Hn=group_Hn;
end

end

