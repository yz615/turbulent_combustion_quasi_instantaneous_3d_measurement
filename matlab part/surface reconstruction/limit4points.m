%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%             FUNCTION CODE                            %
%             limit4points.M                           %
%     Created by Yutao Zheng                           %
%   This function is to identify the portion of        %
%   a certain rectangle inside of the edge             %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function ratio=limit4points(p1,p2,p3,p4,r0)
r=zeros(4,1);
r(1)=sqrt(p1(1)^2+p1(2)^2);
r(2)=sqrt(p2(1)^2+p2(2)^2);
r(3)=sqrt(p3(1)^2+p3(2)^2);
r(4)=sqrt(p4(1)^2+p4(2)^2);

flag=zeros(4,1);
sum=0;
for i=1:1:4
    if r(i)==r0
        flag(i)=0;
    else
        flag(i)=((r(i)-r0)/abs(r(i)-r0)+1)/2;
    end
    sum=flag(i)+sum;
end
flag_bi=flag(1)*1+flag(2)*2+flag(3)*4+flag(4)*8;
switch sum
    case 0
        ratio=1;
    case 1
        r=sort(r);
        ratio=1-1/2*(r(4)-r0)/(r(4)-r(2))*(r(4)-r0)/(r(4)-r(3));
    case 2
        switch flag_bi
            case 12
                ratio=((r0-r(1))/(r(3)-r(1))+(r0-r(2))/(r(4)-r(2)))/2;
            case 10
                ratio=((r0-r(1))/(r(2)-r(1))+(r0-r(3))/(r(4)-r(3)))/2;
            case 3
                ratio=((r0-r(4))/(r(2)-r(4))+(r0-r(3))/(r(1)-r(3)))/2;
            case 5
                ratio=((r0-r(4))/(r(3)-r(4))+(r0-r(2))/(r(1)-r(2)))/2;
        end
    case 3
        r=sort(r);
        ratio=1/2*(r0-r(1))/(r(2)-r(1))*(r0-r(1))/(r(3)-r(1));
    case 4
        ratio=0;
end
if ratio>1
    disp('wrong ratio')
end