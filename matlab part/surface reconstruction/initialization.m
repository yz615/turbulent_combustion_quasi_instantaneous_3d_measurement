%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%             FUNCTION CODE                            %
%               eqn4area.M                             %
%     Created by Yutao Zheng                           %
%   This function convert solve the area among         %
%   four points with certain function 4 order function %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [slice_H,pairs,length]= initialization(h,r,e,stage,p0,listpn,edge,num_sli,pixel)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

%three inputs:h,r,e
%h: the height of the flame(m)
%r: radius of the flame(m)
%e: randomness level, representing the level of displacement fluctuation(m)

%one output:H
%H: discretilized height of the flame over the whole field
%H: N*M*3
%N: the length(num)
%M: the width(num)
%3:(x,y,h) three coordinates of the flame surface(m)

%start
%h=20*10^-3;%20 mm
%r=7*10^-3;%7 mm
%e=1*10^-3;%1 mm
    switch stage
        case 1
            %model for stage 1
            N=101;
            M=101;
            H=zeros(N,M,3);

            res=0.2;%resolution=0.2 mm
            
            %random factors
            peakpoint=[-0.3 0.7; rand(1) -0.9];

            for i=1:1:N
                for j=1:1:M
                    x=(j-51)*res;
                    y=(i-51)*res;
                    H(i,j,1)=x;
                    H(i,j,2)=y;
                    if x^2+y^2<r^2
                        H(i,j,3)=h*(r-sqrt(x^2+y^2))/r;
                        %H(i,j,3)=H(i,j,3)*(1+e*(sqrt(x^2+y^2)-r)*(x-r*peakpoint(1,1))*(x-r*peakpoint(2,1))*(y-r*peakpoint(1,2))*(y-r*peakpoint(2,2))/r^5);
                        %H(i,j,3)=H(i,j,3)-h*(r-sqrt(x^2+y^2))/r;
                    end
                end
            end
            figure(1)
            surf(H(:,:,1),H(:,:,2),H(:,:,3),'Edgecolor','none')
            axis([-10 10 -10 10 -10 30])
            axis equal
            xlabel('x(mm)')
            ylabel('y(mm)')
            zlabel('h(mm)')
            
            slice_H=zeros(pixel,2,num_sli);
            for k=1:1:num_sli
                pn=listpn(k,:);
                line_x=H(1,:,1);
                line_y=H(:,1,2);

                res=H(1,2,1)-H(1,1,1);
                if res==0
                    disp('wrong resolution!')
                end
                gap_y=(edge(2)-edge(1))/pixel;
                x=edge(1)+gap_y/2:gap_y:edge(2)-gap_y/2;
                %gap_y=gap_x*(pn(2)-p0(2))/(pn(1)-p0(1));
                if pn(1)-p0(1)==0
                    y=p0(2)*ones(1,pixel);
                else
                    y=p0(2)*ones(1,pixel)+(pn(2)-p0(2))*(x-p0(1)*ones(1,pixel))/(pn(1)-p0(1));
                end

                h_out=zeros(pixel,1);
                for j=1:1:pixel
                    if x(j)^2+y(j)^2<r^2
                        h_out(j)=h*(r-sqrt(x(j)^2+y(j)^2))/r;
                    end
                end
                pixel_num=1:1:pixel;
                slice_H(:,:,k)=[pixel_num' h_out];
                figure(2)
                plot(pixel_num,h_out)
                hold on
            end
            pairs=ones(num_sli,1);
            length=pixel*ones(num_sli,1);
            %figure(2)
            %for i=1:2:num_sli
            %   plot(slice_H(:,1,i),slice_H(:,2,i))
            %   hold on
            %end
        case 2
            %model for stage 2
            %an ellipe is added into the cone surface
            %center of ellipse (0,0,h2)
            hp=h*0.7;
            a=r*(1-0.7)*0.8;
            b=r*(1-0.7)*2;
            boff=b*0.1;
            c=h*0.2;
            N=101;
            M=101;
            H=zeros(N,M,3);
            H2=zeros(N,M,3);
            H3=zeros(N,M,3);
            H2(:,:,2)=ones(M,1)*(-b+boff:2*b/(N-1):b+boff);
            H3(:,:,2)=ones(M,1)*(-b+boff:2*b/(N-1):b+boff);
            H2(:,:,2)=H2(:,:,2)';
            H3(:,:,2)=H3(:,:,2)';
            for i=1:1:N
                x_d=a*sqrt(1-(H2(i,1,2)-boff)^2/b^2);
                if x_d>0
                    H2(i,:,1)=(-x_d:2*x_d/(M-1):x_d);
                    H3(i,:,1)=(-x_d:2*x_d/(M-1):x_d);
                end
            end
            [H(:,:,1),H(:,:,2)]=meshgrid(-10:0.2:10);
            for i=1:1:N
                for j=1:1:M
                    x=H(i,j,1);
                    y=H(i,j,2);
                    if x^2+y^2<r^2
                        H(i,j,3)=h*(r-sqrt(x^2+y^2))/r;
                    end
                    x=H2(i,j,1);
                    y=H2(i,j,2);
                    if (x/a)^2+((y-boff)/b)^2<1
                        H2(i,j,3)=hp+c*sqrt(1-(x/a)^2-((y-boff)/b)^2);
                        H3(i,j,3)=hp-c*sqrt(1-(x/a)^2-((y-boff)/b)^2);
                    else
                        H2(i,j,3)=hp;
                        H3(i,j,3)=hp;
                    end
                end
            end
            figure(1)
            surf(H(:,:,1),H(:,:,2),H(:,:,3),'Edgecolor','none')
            hold on
            surf(H2(:,:,1),H2(:,:,2),H2(:,:,3),'Edgecolor','none')
            hold on
            surf(H3(:,:,1),H3(:,:,2),H3(:,:,3),'Edgecolor','none')
            axis equal
            axis([-10 10 -10 10 -10 30])
            xlabel('x(mm)')
            ylabel('y(mm)')
            zlabel('h(mm)')
            %slicing
            max_length=2*pixel;
            temp_length=0;
            slice_Ht=zeros(max_length,2,num_sli);
            p0=[0 100];
            for k=1:1:num_sli
                pn=[listpn(k,2) listpn(k,1)];
                y0marker=100*pn(1)^2/(100^2+pn(1)^2);
                %y0marker=0;
                maxh=0;
                gap_y=(edge(2)-edge(1))/pixel;
                y=edge(1)+gap_y/2:gap_y:edge(2)-gap_y/2;
                %gap_y=gap_x*(pn(2)-p0(2))/(pn(1)-p0(1));
                if pn(1)-p0(1)==0
                    x=p0(1)*ones(1,pixel);
                else
                    x=p0(1)*ones(1,pixel)+(pn(1)-p0(1))*(y-p0(2)*ones(1,pixel))/(pn(2)-p0(2));
                end
                %find the max height
                %something wrong with y0marker
                for i=1:1:pixel
                    if x(i)^2+y(i)^2<r^2
                        
                    end
                end
                
                %x,y are well defined.
                list_p=zeros(max_length,2);
                flag_p=1;
                list_n=zeros(max_length,2);
                flag_n=1;
                %flag_h=0;
                list_out_p=zeros(max_length,2);
                list_out_n=zeros(max_length,2);
                for i=1:1:pixel
                    if x(i)^2+y(i)^2<r^2
                        h_prime=h*(1-sqrt(x(i)^2+y(i)^2)/r);
                        if (x(i)/a)^2+((y(i)-boff)/b)^2>=1
                            if y(i)<y0marker
                                list_n(flag_n,:)=[i h_prime];
                                flag_n=flag_n+1;
                            else
                                list_p(flag_p,:)=[i h_prime];
                                flag_p=flag_p+1;
                            end
                        else
                            h1=hp+c*sqrt(1-(x(i)/a)^2-((y(i)-boff)/b)^2);
                            h2=hp-c*sqrt(1-(x(i)/a)^2-((y(i)-boff)/b)^2);
                            %if h1>h_prime
                            %   flag_h=1;
                            %end
                            if h_prime>h2
                                if y(i)<y0marker
                                    list_n(flag_n,:)=[i max(h1,h_prime)];
                                    flag_n=flag_n+1;
                                else
                                    list_p(flag_p,:)=[i max(h1,h_prime)];
                                    flag_p=flag_p+1;
                                end
                            else
                                if y(i)<y0marker
                                    list_n(flag_n,:)=[i h1];
                                    flag_n=flag_n+1;
                                    list_n(flag_n,:)=[i h2];
                                    flag_n=flag_n+1;
                                    list_n(flag_n,:)=[i h_prime];
                                    flag_n=flag_n+1;
                                else
                                    list_p(flag_p,:)=[i h1];
                                    flag_p=flag_p+1;
                                    list_p(flag_p,:)=[i h2];
                                    flag_p=flag_p+1;
                                    list_p(flag_p,:)=[i h_prime];
                                    flag_p=flag_p+1;
                                end
                            end
                        end
                    else
                        if y(i)<y0marker
                            list_out_n(i,:)=[i 0];
                        else
                            list_out_p(i,:)=[i 0];
                        end
                    end
                end
                list_n(all(list_n==0,2),:)=[];
                list_p(all(list_p==0,2),:)=[];
                list_out_n(all(list_out_n==0,2),:)=[];
                list_out_p(all(list_out_p==0,2),:)=[];
                list_n=sortrows(list_n,2);
                list_p=sortrows(list_p,-2);
                list_out_n=sortrows(list_out_n,1);
                list_out_p=sortrows(list_out_p,1);
                list=[list_out_n;list_n;list_p;list_out_p;];
                temp_length=max([temp_length max(size(list))]);
                slice_Ht(1:max(size(list)),:,k)=list;
                %figure(2)
                %plot(list(:,1),list(:,2))
                %hold on
            end
            slice_H=slice_Ht(1:temp_length,:,:);
            pairs=ones(temp_length,1);
            length=temp_length*ones(temp_length,1);
        case 3
            %model for stage 3
            hp=1.5*h;
            %center=[0 0 hp];
            rad=h*0.1;%somthing wrong
            
            N=101;
            M=101;
            H=zeros(N,M,3);
            H2=zeros(N,M,3);
            H3=zeros(N,M,3);
            
            res=0.2;%resolution=0.2 mm
            
            %random factors
            peakpoint=[-0.3 0.7; rand(1) -0.9];
            H2(:,:,1)=ones(M,1)*(-rad:2*rad/(N-1):rad);
            H3(:,:,1)=ones(M,1)*(-rad:2*rad/(N-1):rad);
            for i=1:1:N
                y_d=sqrt(rad^2-H2(1,i,1)^2);
                if y_d>0
                    H2(:,i,2)=-y_d:2*y_d/(M-1):y_d;
                    H3(:,i,2)=-y_d:2*y_d/(M-1):y_d;
                end
            end
            for i=1:1:N
                for j=1:1:M
                    x=(j-51)*res;
                    y=(i-51)*res;
                    H(i,j,1)=x;
                    H(i,j,2)=y;
                    if x^2+y^2<r^2
                        H(i,j,3)=h*(r-sqrt(x^2+y^2))/r;
                    end
                    if H2(i,j,1)^2+H2(i,j,2)^2<rad^2
                        H2(i,j,3)=hp+sqrt(rad^2-H2(i,j,1)^2-H2(i,j,2)^2);
                        H3(i,j,3)=hp-sqrt(rad^2-H2(i,j,1)^2-H2(i,j,2)^2);
                    else
                        H2(i,j,3)=hp;
                        H3(i,j,3)=hp;
                    end
                end
            end
            figure(1)
            surf(H(:,:,1),H(:,:,2),H(:,:,3),'Edgecolor','none')
            axis([-10 10 -10 10 -10 50])
            hold on
            surf(H2(:,:,1),H2(:,:,2),H2(:,:,3),'Edgecolor','none')
            hold on
            surf(H3(:,:,1),H3(:,:,2),H3(:,:,3),'Edgecolor','none')
            axis equal
            axis([-10 10 -10 10 -10 50])
            xlabel('X(mm)')
            ylabel('Y(mm)')
            zlabel('H(mm)')
            
            
            slice_H=zeros(pixel,2,num_sli,2);
            pairs=ones(num_sli,1);
            length=pixel*ones(num_sli,1);
            for k=1:1:num_sli
                pn=listpn(k,:);
                line_x=H(1,:,1);
                line_y=H(:,1,2);

                res=H(1,2,1)-H(1,1,1);
                if res==0
                    disp('wrong resolution!')
                end
                gap_y=(edge(2)-edge(1))/pixel;
                x=edge(1)+gap_y/2:gap_y:edge(2)-gap_y/2;
                %gap_y=gap_x*(pn(2)-p0(2))/(pn(1)-p0(1));
                if pn(1)-p0(1)==0
                    y=p0(2)*ones(1,pixel);
                else
                    y=p0(2)*ones(1,pixel)+(pn(2)-p0(2))*(x-p0(1)*ones(1,pixel))/(pn(1)-p0(1));
                end

                h_out=zeros(pixel,1);
                for j=1:1:pixel
                    if x(j)^2+y(j)^2<r^2
                        h_out(j)=h*(r-sqrt(x(j)^2+y(j)^2))/r;
                    end
                end
                pixel_num=1:1:pixel;
                slice_H(:,:,k,1)=[pixel_num' h_out];
                figure(2)
                plot(pixel_num,h_out)
                hold on
            
                start=1;
                temp1=zeros(pixel,2);
                temp2=zeros(pixel,2);
                for i=1:1:pixel
                    ix=floor(bisection(x(i),line_x));
                    iy=floor(bisection(y(i),line_y));
                    if x(i)^2+y(i)^2==rad^2
                        %slice_H(start,:,k,2)=[i,hp];
                        temp1(start,:)=[i hp];
                        start=start+1;
                    else
                        if x(i)^2+y(i)^2<rad^2
                            %slice_H(start,:,k,2)=[i,hp+sqrt(rad^2-ix^2-iy^2)];
                            temp1(start,:)=[i hp+sqrt(rad^2-x(i)^2-y(i)^2)];
                            %start=start+1;
                            %slice_H(start,:,k,2)=[i,hp-sqrt(rad^2-ix^2-iy^2)];
                            temp2(start,:)=[i hp-sqrt(rad^2-x(i)^2-y(i)^2)];
                            start=start+1;
                        end
                    end
                end
                temp1(all(temp1==0,2),:)=[];
                temp2(all(temp2==0,2),:)=[];
                temp_s=size(temp1);
                if temp_s(1)>0
                    temp1=sortrows(temp1,1);
                    temp2=sortrows(temp2,-1);
                    temp=[temp1;temp2;temp1(1,:)];
                    slice_H(1:max(size(temp)),:,k,2)=temp;
                    plot(temp(:,1),temp(:,2));
                    %hold on
                    %scatter(temp(:,1),temp(:,2));
                    hold on
                    pairs(k,1)=2;
                end
            end
    end
end

