%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%             bisection.M FUNCTION CODE                %
%     Created by Yutao Zheng                           %
%   This function seek the mid point on a line_x       %
%   through using binary section method                %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%% Function Header Cell
% Establishes the passing parameters

% Inputs
% x : target value
% line_x : target line of x


% Outputs
% y : the serial number in the line_x where its value is equal to x

function y=bisection(x,line_x)

num=max(size(line_x));
p=1;
q=num;
%r=0.618;

if (line_x(num)-x)*(line_x(1)-x)>0
    y=-1;
else
    mid=floor(p+(q-p)/2);
    while q-p>1
        if (line_x(mid)-x)*(line_x(p)-x)<0
            q=mid;
        else
            if line_x(mid)==x
                q=mid;
            else
                p=mid;
            end
        end
        mid=floor(p+(q-p)/2);
    end
    q=p+1;
    if q-p>1
        disp('q-p>1!')
    else
        if (line_x(q)-x)*(line_x(p)-x)<0
            y=p+0.5;
        else
            if (line_x(q)-x)*(line_x(p)-x)>0
                disp('Wrong with p and q!')
            else
                if line_x(q)==x
                    y=q;
                else
                    if line_x(p)==x
                    y=p;
                    end
                end
            end
        end
    end
end