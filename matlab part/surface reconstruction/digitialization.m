%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%             FUNCTION CODE                            %
%        digitialization.M                             %
%     Created by Yutao Zheng                           %
%   This function convert a line data                  %
%   directly from the camera data to a                 %
%   slicing data based on the location of this slice.  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%% Function Header Cell
% Establishes the passing parameters

%four inputs
%slice_H: picture from the camera,(num,height),two pixel number
%p0 and pn: line for the location, p0(x0,y0) and pn(xn,yn)
%si: number of slice

%one output
%line_H: a matrix num*3, (x,y,height)

function line_H = digitialization(slice_H,p0,pn,edge,pixel)
%start
%x direction: parallel to the camera view
%y direction: perpendicular to the camear view
temp=size(slice_H);
%disp(temp)
length=temp(1);
gap_y=(edge(2)-edge(1))/pixel;
if max(size(temp))==2
    %stage==1 or only one line in stage 2 and 3
    %one output:line_H
    %line_H:pixel*3
    %3:(x,y,h) three coordinates of the flame surface(m)
    x=zeros(length,1);
    y=zeros(length,1);
    for j=1:1:length
        if slice_H(j,1)>0
            y(j)=edge(1)+(slice_H(j,1)-0.5)*gap_y;
            if pn(1)-p0(1)==0
                x(j)=p0(2);
            else
                x(j)=p0(2)+(pn(2)-p0(2))*(y(j)-p0(1))/(pn(1)-p0(1));
            end
        end
        %disp(size(slice_H))
    end
    line_H=[x y slice_H(:,2)];
else
    if max(size(temp))>2
        %stage is 2 or 3 with multiple lines here
        %there might be multiple lines
        %sline_H:num*2*n; n is the number of lines in this stage
        %n can be measured by the function size
        %output:line_H:max_length*3
        n=temp(3);
        line_H=zeros(length,3,n);
        for i=1:1:n
            x=zeros(length,1);
            y=zeros(length,1);
            for j=1:1:length
                y(j)=edge(1)+(slice_H(j,1,i)-0.5)*gap_y;
                if pn(1)-p0(1)==0
                    x(j)=p0(2);
                else
                    x(j)=p0(2)+(pn(2)-p0(2))*(y(j)-p0(1))/(pn(1)-p0(1));
                end
            end
            %disp(size(slice_H))
            line_H=[x y slice_H(:,2,i)];
        end
    end
end

