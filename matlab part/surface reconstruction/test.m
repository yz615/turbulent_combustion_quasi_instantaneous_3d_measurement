clc
clear
p1=[0 0 0];
p2=[0 0 3];
p3=[3 0 0];
p4=[3 0.5 3];
x=[p1(1) p2(1) p3(1) p4(1)];
y=[p1(2) p2(2) p3(2) p4(2)];
h=[p1(3) p2(3) p3(3) p4(3)];
scatter3(x,y,h)
plot3(x,y,h)
axis equal
area_xy=(triarea(p1(1:2),p2(1:2),p3(1:2))+triarea(p2(1:2),p3(1:2),p4(1:2))+triarea(p1(1:2),p2(1:2),p4(1:2))+triarea(p1(1:2),p3(1:2),p4(1:2)))/2;
area=(triarea(p1,p2,p3)+triarea(p2,p3,p4)+triarea(p1,p2,p4)+triarea(p1,p3,p4))/2;
ratio=area/area_xy
factor=eqn4solve(p1,p2,p3,p4)
phps=factor(1)*0.5+factor(2);
phpt=factor(1)*0.5+factor(3);
limit=10^-2;
if abs(p3(1)-p1(1)+p4(1)-p2(1))<limit
    pspx=0;%10^3*sign(p3(1)-p1(1)+p4(1)-p2(1));
else
    pspx=(1/(p3(1)-p1(1)+p4(1)-p2(1)))*2;
end
        if abs(p3(2)-p1(2)+p4(2)-p2(2))<limit
            pspy=0;%10^3*sign(p3(2)-p1(2)+p4(2)-p2(2));
        else
            pspy=(1/(p3(2)-p1(2)+p4(2)-p2(2)))*2;
        end
        if abs(p2(1)-p1(1)+p4(1)-p3(1))<limit
            ptpx=0;%10^3*sign(p2(1)-p1(1)+p4(1)-p3(1));
        else
            ptpx=(1/(p2(1)-p1(1)+p4(1)-p3(1)))*2;
        end
        if abs(p2(2)-p1(2)+p4(2)-p3(2))<limit
            ptpy=0;%10^3*sign(p2(2)-p1(2)+p4(2)-p3(2));
        else
            ptpy=(1/(p2(2)-p1(2)+p4(2)-p3(2)))*2;
        end
phpx=phps*pspx+phpt*ptpx;
phpy=phps*pspy+phpt*ptpy;
dadxdy=sqrt(1+phpx^2+phpy^2);
surfaceone(factor,p1,p2,p3,p4,4)