%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%             FUNCTION CODE                            %
%               triarea.M                              %
%     Created by Yutao Zheng                           %
%   This function calculate the area of a triangle     %
%   with three points                                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function area=triarea(p1,p2,p3)
if length(p1)==2
    %for 2D planar triangle
    %p1(x1,y1),p2(x2,y2),p3(x3,y3);
    a=sqrt((p2(1)-p1(1))^2+(p2(2)-p1(2))^2);
    b=sqrt((p3(1)-p1(1))^2+(p3(2)-p1(2))^2);
    c=sqrt((p3(1)-p2(1))^2+(p3(2)-p2(2))^2);
    p=(a+b+c)/2;
    area=sqrt(p*(p-a)*(p-b)*(p-c));
else
    if length(p1)==3
        %for 3D triangle
        a=sqrt((p2(1)-p1(1))^2+(p2(2)-p1(2))^2+(p2(3)-p1(3))^2);
        b=sqrt((p3(1)-p1(1))^2+(p3(2)-p1(2))^2+(p3(3)-p1(3))^2);
        c=sqrt((p3(1)-p2(1))^2+(p3(2)-p2(2))^2+(p3(3)-p2(3))^2);
        p=(a+b+c)/2;
        area=sqrt(p*(p-a)*(p-b)*(p-c));
    else
        area=0;
    end
end