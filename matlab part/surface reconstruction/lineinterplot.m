%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%             FUNCTION CODE                            %
%               lineinterplot.M                        %
%     Created by Yutao Zheng                           %
%   This function is to make the interpolation based   %
%   on a line of data and convert them into a new line %
%   of data with a certain length, sec.                %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [output,area]=lineinterplot(line,N,sec)
%this function will re-interplot the line and measure the length of the line

Size=size(line);
index=Size(1);
line(all(line==0,2),:)=[];
Size_reform=size(line);
index_reform=Size_reform(1);
area=0;
if index_reform==0
    output=zeros(sec,4);
    area=0;
else
    if index_reform<3
        output=0;
        area=-1;
    else
        num=1:1:index_reform;
        num_e=1:1/N:index_reform;%N is the number of the section between each two points
        xe=interp1(num,line(:,1),num_e','spline');
        ye=interp1(num,line(:,2),num_e','spline');
        ze=interp1(num,line(:,3),num_e','spline');
        se=zeros(max(size(xe)),1);
        for i=2:1:max(size(xe))
            se(i)=se(i-1)+sqrt((xe(i)-xe(i-1))^2+(ye(i)-ye(i-1))^2+(ze(i)-ze(i-1))^2);
            %the area calculation assume that y is always increasing.
            area=area+(ze(i-1)+ze(i))/2*sqrt((xe(i)-xe(i-1))^2+(ye(i)-ye(i-1))^2)*sign(ye(i)-ye(i-1));
        end
        max_s=max(se);
        s=0:max_s/(sec-1):max_s;
        x=interp1(se,xe,s');
        y=interp1(se,ye,s');
        z=interp1(se,ze,s');
        output=[x y z s'];
    end
end