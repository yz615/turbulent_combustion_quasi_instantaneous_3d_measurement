%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%             FUNCTION CODE                            %
%               eqn4solve.M                            %
%     Created by Yutao Zheng                           %
%   This function solve the factor of 4 order          %
%   polynomial function with initial four points       %
%   (x,y) will be normalzied into (s,t)=(0~1,0~1)      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function factor=eqn4solve(p1,p2,p3,p4)
%each p should be (x,y,h)
%h=ast+bs+ct+d
%solve A[a;b;c;d]=h
A=[0 0 0 1;
    0 1 0 1;
    0 0 1 1;
    1 1 1 1;];
h=[p1(3);p2(3);p3(3);p4(3);];
%if abs(det(A))<10^-25
%    disp('detA==0')
%    disp([p1(1) p1(2) p1(3)])
%    disp([p2(1) p2(2) p2(3)])
%    disp([p3(1) p3(2) p3(3)])
%    disp([p4(1) p4(2) p4(3)])
%    B1=A(2:4,2:4);
%    h1=h(2:4);
%    if abs(det(B1))<10^-25
%        factor=zeros(4,1);
%    else
%        factor=zeros(4,1);
%        factor(2:4)=B1^(-1)*h1;
%    end
%else
    factor=Ni(A)*h;
%end
%check
%a=factor(1);
%b=factor(2);
%c=factor(3);
%d=factor(4);
%flag=0;
%if abs(sum(abs(A*factor)))>10^-8
%    if p1(3)~=a*p1(1)*p1(2)+b*p1(1)+c*p1(2)+d
%        disp('WRONG 1!')
%        flag=1;
%        disp(sqrt(p1(1)^2+p1(2)^2))
%    end
%    if p2(3)~=a*p2(1)*p2(2)+b*p2(1)+c*p2(2)+d
%        disp('WRONG 2!')
%        flag=1;
%        disp(sqrt(p2(1)^2+p2(2)^2))
%    end
%    if p3(3)~=a*p3(1)*p3(2)+b*p3(1)+c*p3(2)+d
%        disp('WRONG 3!')
%        flag=1;
%        disp(sqrt(p3(1)^2+p3(2)^2))
%    end
%    if p4(3)~=a*p4(1)*p4(2)+b*p4(1)+c*p4(2)+d
%        disp('WRONG 4!')
%        flag=1;
%        disp(sqrt(p3(1)^2+p3(2)^2))
%    end
%    if flag==1
%        disp(A)
%        disp(h)
%        disp(A*factor-h)
%    end
%end