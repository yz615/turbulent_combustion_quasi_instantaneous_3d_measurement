%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%             FUNCTION CODE                            %
%               slicing.M                              %
%     Created by Yutao Zheng                           %
%   This function convert initialzied data to a series %
%   of slicing data                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function slice_H=slicing(H,p0,pn,edge,pixel)
%This function is to make a series of slices here based on interplotation.
%This function initialzation also contains this function.
%This function is not useful under normal conditions.

%five inputs
%H: surface information from the function initialiation
%p0 and pn: line for the location, p0(x0,y0) and pn(xn,yn)
%edge: camear edge, 1*2, limit of x
%pixel: number of pixel


%one output:slice_H
%slice_H: discretilized height of the flame over this camera frame
%slice_H: pixel*2
%pixel: number of pixel for the camera
%3:(num_pixel,h) three coordinates of the flame surface(m)

%start
%
%slice_H=zeros(pixel,2);
line_x=H(1,:,1);
line_y=H(:,1,2);
    
res=H(1,2,1)-H(1,1,1);
if res==0
    disp('wrong resolution!')
end
gap_x=(edge(2)-edge(1))/pixel;
x=edge(1)+gap_x/2:gap_x:edge(2)-gap_x/2;
%gap_y=gap_x*(pn(2)-p0(2))/(pn(1)-p0(1));
if pn(1)-p0(1)==0
    y=p0(2)*ones(1,pixel);
else
    y=p0(2)*ones(1,pixel)+(pn(2)-p0(2))*(x-p0(1)*ones(1,pixel))/(pn(1)-p0(1));
end

h_out=zeros(pixel,1);
for i=1:1:pixel
    ix=floor(bisection(x(i),line_x));
    iy=floor(bisection(y(i),line_y));
    %disp(x(i))
    %disp(y(i))
    p1=[H(ix,iy,2) H(ix,iy,1) H(ix,iy,3)];
    p2=[H(ix+1,iy,2) H(ix+1,iy,1) H(ix+1,iy,3)];
    p3=[H(ix,iy+1,2) H(ix,iy+1,1) H(ix,iy+1,3)];
    p4=[H(ix+1,iy+1,2) H(ix+1,iy+1,1) H(ix+1,iy+1,3)];
    %disp(p1)
    %disp(p2)
    %disp(p3)
    %disp(p4)
    factor=eqn4solve(p1,p2,p3,p4);
    %disp(factor)
    h_out(i,1)=factor(1)*x(i)*y(i)+factor(2)*x(i)+factor(3)*y(i)+factor(4);
end
pixel_num=1:1:pixel;
slice_H=[pixel_num' h_out];

end

