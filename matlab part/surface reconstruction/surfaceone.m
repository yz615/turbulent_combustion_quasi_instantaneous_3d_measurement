%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%             FUNCTION CODE                            %
%             surfaceone.M                             %
%     Created by Yutao Zheng                           %
%   This function visualizes the surface with four     %
%   points and their corresponding factor of equations %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function surfaceone(f,p1,p2,p3,p4,mode2)

%p1~p4 define the edge
%factor defines the surface as factors of the normalzied function 

%3*3 matrix will plot this small surf
%p1:i,j
%p2:i+1,j
%p3:i,j+1
%p4:i+1,j+1
switch mode2
    case 4
        N=4;
        x=zeros(N,N);
        y=zeros(N,N);
        h=zeros(N,N);
        x(1,1)=p1(1);
        x(N,1)=p2(1);
        x(1,N)=p3(1);
        x(N,N)=p4(1);
        y(1,1)=p1(2);
        y(N,1)=p2(2);
        y(1,N)=p3(2);
        y(N,N)=p4(2);
        for i=1:1:N
            for j=1:1:N
                s=(i-1)/(N-1);
                t=(j-1)/(N-1);
                c1=(N-i)/(N-1)*(N-j)/(N-1);
                c2=(N-i)/(N-1)*(j-1)/(N-1);
                c3=(i-1)/(N-1)*(N-j)/(N-1);
                c4=(i-1)/(N-1)*(j-1)/(N-1);
                x(i,j)=x(1,1)*c1+x(1,N)*c2+x(N,1)*c3+x(N,N)*c4;
                y(i,j)=y(1,1)*c1+y(1,N)*c2+y(N,1)*c3+y(N,N)*c4;
                h(i,j)=f(1)*s*t+f(2)*s+f(3)*t+f(4);
                %if h(1,1)~=p1(3)
                    %disp('wrong')
                    %disp(p1)
                %end
                %if h(1,N)~=p2(3)
                    %disp('wrong')
                    %disp(p2)
                %end
                %if h(N,1)~=p3(3)
                    %disp('wrong')
                    %disp(p3)
                %end
                %if h(N,N)~=p4(3)
                    %disp('wrong')
                    %disp(p4)
                %end
            end
        end
        surf(x,y,h,'Edgecolor','none')
        hold on
        axis equal
        axis([-10 10 -10 10 -10 50])
        hold on
        %scatter3(x(:,1),y(:,1),h(:,1));
        %hold on
        %scatter3(x(:,N),y(:,N),h(:,N));
        xlabel('X(mm)')
        ylabel('Y(mm)')
        zlabel('H(mm)')
        hold on
    case 1
        N=2;
        x=zeros(N,N);
        y=zeros(N,N);
        h=zeros(N,N);
        x(1,1)=p1(1);
        x(N,1)=p2(1);
        x(1,N)=p3(1);
        x(N,N)=p4(1);
        y(1,1)=p1(2);
        y(N,1)=p2(2);
        y(1,N)=p3(2);
        y(N,N)=p4(2);
        h(1,1)=p1(3);
        h(N,1)=p2(3);
        h(1,N)=p3(3);
        h(N,N)=p4(3);
        surf(x,y,h,'Edgecolor','none')
        hold on
        axis equal
        axis([-10 10 -10 10 -10 50])
        xlabel('X(mm)')
        ylabel('Y(mm)')
        zlabel('H(mm)')
        hold on
end
