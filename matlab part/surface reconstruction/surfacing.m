%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%             FUNCTION CODE                            %
%               surfacing.M                            %
%     Created by Yutao Zheng                           %
%   This function convert start the surfacing process  %
%   based on two adhesive lines of data. Each one      %
%   should have its (x,y,h) coordinates                %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [surface_f,area]= surfacing(mesh_Hn,mode,mode2,onoff,r,sec,location,f_switch,pairs_j,mode_method)

%two input:
%mesh_Hn: a group of height after being meshed
%mode: structure mesh (0) and unstructure mesh (1)
%mode2: mode of interpolation, (4) for 4-factors equation(xy+x+y+c)

%one output:
%for structure mesh, element num is (ix,iy)
%for unstructure mesh, element num is (i)
%for (mode,mode2)=(0,4), surface_f=number of (N-1)*(pixel-1)*(a1,a2,a3,a4);

%here, to calculate the area, we choose mode 2(middle accuracy)
    size_mesh=size(mesh_Hn);
    %max_length=size_mesh(1);%number of slice
    num_sli=size_mesh(2);% number of pixel
    if num_sli>2
        disp('You may input more than 2 slices in surfacing function')
    end
    
    %area calculation for between these two slices
    area=0;
    
    if mode==0
        if max(pairs_j)==1%only one lines for each slice
            %num_ele=(N-1)*(pixel-1);
            temp_linedata1=[mesh_Hn(:,1,1) mesh_Hn(:,1,2) mesh_Hn(:,1,3)];
            temp_linedata2=[mesh_Hn(:,2,1) mesh_Hn(:,2,2) mesh_Hn(:,2,3)];
            [linedata1,s_area1]=lineinterplot(temp_linedata1,5,sec);
            [linedata2,s_area2]=lineinterplot(temp_linedata2,5,sec);
            [surface_f,area]=singlepair(linedata1,linedata2,sec,mode2,onoff,r);
        else                                                     
            %more lines for each slice
            num_line=max(pairs_j);
            s_area1=zeros(num_line,1);
            s_area2=zeros(num_line,1);
            flag=zeros(num_line,1);
            linedata1=zeros(sec,4,num_line);
            linedata2=zeros(sec,4,num_line);
            for k=1:1:num_line
                temp_linedata1=[mesh_Hn(:,1,1,k) mesh_Hn(:,1,2,k) mesh_Hn(:,1,3,k)];
                temp_linedata2=[mesh_Hn(:,2,1,k) mesh_Hn(:,2,2,k) mesh_Hn(:,2,3,k)];
                [linedata1(:,:,k),s_area1(k)]=lineinterplot(temp_linedata1,5,sec);
                [linedata2(:,:,k),s_area2(k)]=lineinterplot(temp_linedata2,5,sec);
                if s_area1(k)>0
                    flag(k)=flag(k)+1;
                end
                if s_area2(k)>0
                    flag(k)=flag(k)+2;
                end
            end
            if length(find(flag==1))+length(find(flag==2))==0
                %at least number is ok;
                %directly run the surfacing process
                n3=length(find(flag==3));
                if n3>0
                    %start to run surfacing process for each pair of line.
                    for k=1:1:n3
                        [surface_f,area_p]=singlepair(linedata1(:,:,k),linedata2(:,:,k),sec,mode2,onoff,r);
                        area=area+area_p;
                    end
                end
            else
                %find inconsistency for number of line
                if f_switch==0
                    disp('***')
                    disp('Inconsistent number of line between slices ')
                    disp(location)
                    disp('The circumstance of lines here:')
                    disp(flag)
                    disp('***')
                    surface_f=0;
                    area=0;
                else
                    %some methods must have to be done
                    switch mode_method
                        case 1
                            %simply accumluate the area
                            %surface the exact it self
                            n3=length(find(flag==3));
                            if n3>0
                                %start to run surfacing process for each pair of line.
                                for k=1:1:n3
                                    [surface_f,area_p]=singlepair(linedata1(:,:,k),linedata2(:,:,k),sec,mode2,onoff,r);
                                    area=area+area_p;
                                end
                            end
                            if flag(n3+1)==1
                                area=area+s_area1(n3+1);
                                linedata_p=linedata1;
                                linedata_p(all(linedata_p==0,2),:)=[];
                                patch(linedata_p(:,1,n3+1),linedata_p(:,2,n3+1),linedata_p(:,3,n3+1),'yellow');
                            end
                            if flag(n3+1)==2
                                area=area+s_area2(n3+1);
                                linedata_p=linedata2;
                                linedata_p(all(linedata_p==0,2),:)=[];
                                patch(linedata_p(:,1,n3+1),linedata_p(:,2,n3+1),linedata_p(:,3,n3+1),'yellow');
                            end
                        case 2
                            %connect one with another
                            %resurf them like previous one
                        case 3
                            %others
                    end
                end
            end
        end
    end

end

% an element of surfacing process
function [surface_f,area]=singlepair(linedata1,linedata2,sec,mode2,onoff,r)
    surface_f=zeros(sec-1,4);%this 4 might be wrong
    area=0;
    switch mode2
        case 4
            for i=1:1:sec-1
                p1=linedata1(i,:);
                p2=linedata1(i+1,:);
                p3=linedata2(i,:);
                p4=linedata2(i+1,:);
                factor=eqn4solve(p1,p2,p3,p4);
                surface_f(i,:)=factor;
                area=area+eqn4area(surface_f(i,:),p1,p2,p3,p4,2,r);
                if onoff==1
                     surfaceone(factor,p1,p2,p3,p4,mode2);
                end
            end
        case 1
            for i=1:1:sec-1
                p1=linedata1(i,:);
                p2=linedata1(i+1,:);
                p3=linedata2(i,:);
                p4=linedata2(i+1,:);
                factor=eqn4solve(p1,p2,p3,p4);
                surface_f(i,:)=factor;
                area=area+eqn4area(surface_f(i,:),p1,p2,p3,p4,1,r);
                if onoff==1
                     surfaceone(0,p1,p2,p3,p4,mode2);
                end
            end
    end

end
