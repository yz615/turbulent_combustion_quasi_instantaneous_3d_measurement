clc
clear
%start
disp('start time')
datestr(now)
%importing essential factors
L0=2*pi;%length of the field in scanning direction (x direction)
L_y=2*pi;%length of the field in the y direction
t0=1;
u_s=L0/t0;
nu=1.5*10^-5;
nu=nu/L0/u_s;

%main part to apply adjoint method based on measurement flow
%importing data
X= xlsread('data.xlsx','Sheet 1');
Y= xlsread('data.xlsx','Sheet 2');
u0=xlsread('data.xlsx','Sheet 3');
v0=xlsread('data.xlsx','Sheet 4');
um=xlsread('data.xlsx','Sheet 5');
vm=xlsread('data.xlsx','Sheet 6');

[num_y,num_x]=size(X);
x=X(1,:);
x=x';
y=Y(:,1);


%measurement factor
num_t=num_x;%in adjoint part
dt=t0/(num_t-1);
F=zeros(num_y,num_x,num_t);
for j=1:1:num_x
    F(:,j,j)=ones(num_y,1);
end
%re-evolve the flow field
mode1=0;
order=2;
sec=2;
display_onoff=[0 0];%[1 2];


input_uv0=[u0(:);v0(:)];
out_0=evolution_2Din(x,y,input_uv0,nu,num_t,dt,display_onoff,mode1,order,sec);
u=out_0(:,:,:,1);
v=out_0(:,:,:,2);

%start adjoint method
iter=20;
Loss=zeros(10,1);%error between um and calculated measurement

%give a guess
u_start=zeros(num_y,num_x);
v_start=zeros(num_y,num_x);
input_uv_start=[u_start(:);v_start(:)];
[J0,grad0]=calcJ(input_uv_start,x,y,um,vm,F,dt,nu,num_t,mode1,order,sec);

%using fminunc
%set options for fminunc
options = optimoptions(@fminunc,'Display','iter','Algorithm','quasi-newton','MaxIter',20);
[uv_out,cost,exitflag,output]=fminunc(@(input_uv)(calcJ(input_uv,x,y,um,vm,F,dt,nu,num_t,mode1,order,sec)),input_uv_start,options);



%evaluate the initial gradient as a double check through shaking the box
pepu_s=xlsread('gradient_zero.xlsx','sheet1');
pepv_s=xlsread('gradient_zero.xlsx','sheet2');
disp('step into iteration')
datestr(now)
error=zeros(iter,1);
for i=1:1:iter
    out_0=evolution_2Din(x,y,input_uv_start,nu,num_t,dt,display_onoff,mode1,order,sec);
    u=out_0(:,:,:,1);
    v=out_0(:,:,:,2);
    uvhat0=evolution_2Din_adjoint(x,y,u,v,nu,num_t,dt,F,um,vm,mode1,order,sec);
    uhat0=uvhat0(:,:,1);
    vhat0=uvhat0(:,:,2);
    pepu=-uhat0*2*num_x;
    pepv=-vhat0*2*num_x;
    gamma=seekgamma(x,y,u_start,v_start,pepu,pepv,um,vm,dt,F,nu,num_t,mode1,order,sec);
    u_start=u_start-gamma(1)*pepu;
    v_start=v_start-gamma(1)*pepv;
    error(i)=gamma(2);
    figure(2*i+1)
    quiver(x,y,u0,v0);
    hold on
    quiver(x,y,u_start,v_start);
    xlabel('x/L_0')
    ylabel('y/L_0')
    legend('initial','Prediction')
    fname=['step ' num2str(i) ' velocity comparison'];
    title(fname)
    set(gca,'Fontsize',16,'Fontname','Times')
    saveas(gcf,fname)
    figure(2*i+2)
    contourf(X,Y,sqrt((u_start-u0).^2+(v_start-v0).^2))
    colorbar
    xlabel('x/L_0')
    ylabel('y/L_0')
    fname=['step ' num2str(i) ' error contour'];
    title(fname)
    set(gca,'Fontsize',16,'Fontname','Times')
    saveas(gcf,fname)
    disp(['iteration ' num2str(i) ' is done.'])
    datestr(now)
end


