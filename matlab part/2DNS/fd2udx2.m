function y=fd2udx2(u,x,mode1,order)
%this function is to differetiate a certain variabkle two d2u/dx2

N_u=size(u);
N_x=size(x);
if N_u(1)~=N_x(1)
    disp('Inequality between the u and x in function fd2udx2')
else
    N=N_x(1);
    d2udx2=zeros(N_u(1),N_u(2));
    if mode1==0
        switch order
            case 4
                for i=3:1:N-2
                    dx0=x(i,:)-x(i-1,:);
                    dx2=x(i+1,:)-x(i,:);
                    d2udx2(i,:)=(16*u(i+1,:)+16*u(i-1,:)-30*u(i,:)-u(i+2,:)-u(i-2,:))/6/(dx2^2+dx0^2);
                end
                d2udx2(1,:)=(11*u(5,:)-56*u(4,:)+114*u(3,:)-104*u(2,:)+35*u(1,:))/12/(x(2,:)-x(1,:))^2;%4 order
                d2udx2(2,:)=(6*u(3,:)+11*u(1,:)-20*u(2,:)+4*u(4,:)-u(5,:))/12/(x(3,:)-x(2,:))^2;%4 order
                d2udx2(N,:)=(11*u(N-4,:)-56*u(N-3,:)+114*u(N-2,:)-104*u(N-1,:)+35*u(N,:))/12/(x(N,:)-x(N-1,:))^2;%4 order
                d2udx2(N-1,:)=(6*u(N-2,:)+11*u(N,:)-20*u(N-1,:)+4*u(N-3,:)-u(N-4,:))/12/(x(N,:)-x(N-1,:))^2;%4 order
            case 2
                for i=2:1:N-1
                    dx0=x(i,:)-x(i-1,:);
                    dx2=x(i+1,:)-x(i,:);
                    d2udx2(i,:)=2*(u(i+1,:)+u(i-1,:)-2*u(i,:))/(dx2^2+dx0^2);
                end
                d2udx2(1,:)=(-u(4,:)+4*u(3,:)-5*u(2,:)+2*u(1,:))/(x(2,:)-x(1,:))^2;%3 order
                d2udx2(N,:)=(-u(N-3,:)+4*u(N-2,:)-5*u(N-1,:)+2*u(N,:))/(x(N,:)-x(N-1,:))^2;%3 order
        end
    end
end
y=d2udx2;