clc
clear

model = createpde();

SQ1 = [3,4,0,1,1,0,1,1,0,0]';
%C1 = [1,0.5,-0.25,0.25]';
%C1 = [C1;zeros(length(R1) - length(C1),1)];
%gm = [R1,C1];
%sf = 'R1-C1';

%ns = char('R1','C1');
%ns = ns';
%g = decsg(gm,sf,ns);

g=decsg(SQ1);
geometryFromEdges(model,g);
pdegplot(model,'EdgeLabels','on')

ylim([-0.1,1.1])
xlim([-0.1,1.1])
axis equal

a=2;
b=1.5;
x=0:1/22:1;
y=0:1/15:1;
[X,Y]=meshgrid(x,y);
f=zeros(length(y),length(x));
for i=1:1:length(x)
    for j=1:1:length(y)
        f(j,i)=-(a^2+b^2)*pi^2*sin(a*pi*x(i))*sin(b*pi*y(j));
    end
end
fun=@(location,state)interp2(X,Y,f,location.x,location.y);

applyBoundaryCondition(model,'dirichlet','Edge',1:model.Geometry.NumEdges,'u',0);
specifyCoefficients(model,'m',0,'d',0,'c',1,'a',0, 'f',fun);
generateMesh(model,'Hmax',0.1);
results = solvepde(model);
figure(1)
pdeplot(model,'XYData',results.NodalSolution)
querypoints = [X(:),Y(:)]';
pintrp=interpolateSolution(results,querypoints);
pintrp = reshape(pintrp,size(X));
figure(2)
contourf(X,Y,pintrp,30)
colorbar



