function out=evolution_2Din(x,y,input_uv,nu,num_t,dt,display_onoff,mode1,order,sec)
% this function is to calculate the evolution of 1D incompressible flow
% nu is the viscosity
% num is the number of time iteration,num does not consider the initial 0
% dt is the time step
% sec is the number of section between each time step; otherwise, it won't
% converge on time.
num_x=length(x);
num_y=length(y);

u0=reshape(input_uv(1:num_x*num_y),num_y,num_x);
v0=reshape(input_uv(num_x*num_y+1:2*num_x*num_y),num_y,num_x);

N_u=size(u0);
N_v=size(v0);

if display_onoff(2)~=0
        figure(display_onoff(2))
        ylabel('log(Var(RHS))')
        xlabel('iteration')
end

if min([num_x N_u(2) N_v(2)])==max([num_x N_u(2) N_v(2)])
    if min([num_y N_u(1) N_v(1)])==max([num_y N_u(1) N_v(1)])
        flag=0;
    else
        flag=1;
    end
else
    flag=1;
end

if flag==1
    disp('Inequality between the u and x in function evolution_2Din')
else
    u=zeros(num_y,num_x,num_t);
    v=zeros(num_y,num_x,num_t);
    u(:,:,1)=u0;
    v(:,:,1)=v0;
    for i=2:1:num_t
        uv_out=evolution_sec(u(:,:,i-1),v(:,:,i-1),x,y,sec,dt,nu,mode1,order,display_onoff);
        u(:,:,i)=uv_out(:,:,1);
        v(:,:,i)=uv_out(:,:,2);
        %disp(num2str(i))
    end
end

if display_onoff(1)~=0
    [mesh_X,mesh_Y]=meshgrid(x,y);
    figure(display_onoff(1))
    quiver(mesh_X,mesh_Y,u0,v0)
    xlabel('x/L_0')
    ylabel('y/L_0')
    hold on
    quiver(mesh_X,mesh_Y,u(:,:,num_t),v(:,:,num_t))
    legend('Initial','End')
end
out=zeros(num_y,num_x,num_t,2);
out(:,:,:,1)=u;
out(:,:,:,2)=v;

end

function out=evolution_sec(u_p,v_p,x,y,sec,dt,nu,mode1,order,display_onoff)
    num_x=length(x);
    num_y=length(y);
    %dx=x(2,1)-x(1,1);
    %dy=y(2,1)-y(1,1);
    u=zeros(num_y,num_x,2);
    v=zeros(num_y,num_x,2);

    %starting from zero P distribution
    P0=zeros(num_y,num_x);
    iter=30;
    for t=1:1:sec
        RHSvar=zeros(iter,1);
        u(:,:,1)=u_p;
        v(:,:,1)=v_p;
        
        d2udx2=fd2udx2(u_p',x,mode1,order);
        d2udx2=d2udx2';
        d2vdx2=fd2udx2(v_p',x,mode1,order);
        d2vdx2=d2vdx2';
        d2udy2=fd2udx2(u_p,y,mode1,order);
        d2vdy2=fd2udx2(v_p,y,mode1,order);
        
        du2dx=fdudx(u_p'.^2,x,mode1,order);
        du2dx=du2dx';
        duvdx=fdudx(u_p'.*v_p',x,mode1,order);
        duvdx=duvdx';
        duvdy=fdudx(u_p.*v_p,y,mode1,order);
        dv2dy=fdudx(v_p.^2,y,mode1,order);
        
        for k=1:1:iter
            Ps=P0;%p*
            dpdx=fdudx(Ps',x,mode1,order);
            dpdx=dpdx';
            dpdy=fdudx(Ps,y,mode1,order);
            
            u(:,:,2)=u(:,:,1)+dt/sec*(-du2dx-duvdy-dpdx+nu*d2udx2+nu*d2udy2);
            v(:,:,2)=v(:,:,1)+dt/sec*(-duvdx-dv2dy-dpdy+nu*d2vdx2+nu*d2vdy2);
            us=u(:,:,2);%rho u*
            vs=v(:,:,2);
            
            dusdx=fdudx(us',x,mode1,order);
            dusdx=dusdx';
            dvsdy=fdudx(vs,y,mode1,order);
            RHS=(dusdx+dvsdy)/(dt/sec);
            pp=Poissoneqn(x,y,RHS,'z');
            P0=Ps+pp;
            RHSvar(k,1)=var(var(RHS(2:end,2:end)));
        end
        u_p=u(:,:,2);
        v_p=v(:,:,2);
        P0=Ps;
        if display_onoff(2)~=0
            figure(display_onoff(2))
            plot(log(RHSvar))
            hold on
        end
    end
    out=zeros(num_y,num_x,2);
    out(:,:,1)=us;
    out(:,:,2)=vs;
end

