function [J,grad]=calcJ(input_uvg,x,y,um,vm,F,dt,nu,num_t,mode1,order,sec)
%this function is to calculate the error between guess and measurement
%u_g: guess of the velocity
num_x=length(x);
num_y=length(y);
display_onoff=[0 0];
uvpred=evolution_2Din(x,y,input_uvg,nu,num_t,dt,display_onoff,mode1,order,sec);
u_pred1=uvpred(:,:,:,1).*F;
v_pred1=uvpred(:,:,:,2).*F;
u_pred=zeros(num_y,num_x);
v_pred=zeros(num_y,num_x);
for j=1:1:num_x
    u_pred(:,j)=u_pred1(:,j,j);
    v_pred(:,j)=v_pred1(:,j,j);
end
f_int=ones(num_y,num_x);
f_int(1,:)=1/2*ones(1,num_x);
f_int(num_y,:)=1/2*ones(1,num_x);
f_int(:,1)=1/2*ones(num_y,1);
f_int(:,num_x)=1/2*ones(num_y,1);
J=sum(sum(f_int.*((u_pred-um).*(u_pred-um)+(v_pred-vm).*(v_pred-vm))));

%calculate the gradient
uvhat0=evolution_2Din_adjoint(x,y,u_pred1,v_pred1,nu,num_t,dt,F,um,vm,mode1,order,sec);
uhat0=uvhat0(:,:,1);
vhat0=uvhat0(:,:,2);
pepu=-uhat0*2*num_x;
pepv=-vhat0*2*num_x;
grad=[pepu(:);pepv(:)];