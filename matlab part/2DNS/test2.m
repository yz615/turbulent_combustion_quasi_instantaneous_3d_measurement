%%%%????u=sin(pi*x)*sin(pi*y)??%%%
clc
clear
x=0:1/22:1;
y=0:1/15:1;
[X,Y]=meshgrid(x,y);
a=2;
b=2;
u=sin(a*pi*X).*sin(b*pi*Y);
%%%%??f=-2*pi^2*sin(pi*x)*sin(pi*y)??%%%%%%
f=zeros(length(y),length(x));
for i=1:1:length(x)
    for j=1:1:length(y)
        f(j,i)=-(a^2+b^2)*pi^2*sin(a*pi*x(i))*sin(b*pi*y(j));
    end
end

out=Poissoneqn(x',y',f,'x');

%out=reshape(out,[length(y),length(x)]);
figure(1)
surf(X,Y,out)
axis([0 1 0 1 -1 1])
figure(2)
surf(X,Y,u)
axis([0 1 0 1 -1 1])
xlabel('x')
ylabel('y')
figure(3)
surf(x,y,u-out)