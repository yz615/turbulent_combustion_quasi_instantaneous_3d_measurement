function uprime=evolution_2Din_adjoint(x,y,u,v,nu,num,dt,F,um,vm,mode1,order,sec)
%this function is to calculate the evolution of adjoint variable back side

N_u=size(u);
N_v=size(v);
num_x=length(x);
num_y=length(y);
FpFu=u.*F;
FpFv=v.*F;
for i=1:1:num
    FpFu(:,i,i)=FpFu(:,i,i)-um(:,i);
    FpFv(:,i,i)=FpFv(:,i,i)-vm(:,i);
end
if min([num_x N_u(2) N_v(2)])==max([num_x N_u(2) N_v(2)])
    if min([num_y N_u(1) N_v(1)])==max([num_y N_u(1) N_v(1)])
        flag=0;
    else
        flag=1;
    end
else
    flag=1;
end

if flag==1
    disp('Inequality between the u and x in function evolution_1Din')
else
    uhat=zeros(num_y,num_x,num);
    vhat=zeros(num_y,num_x,num);
    for i=num-1:-1:1
        %previous term
        display_onoff=[i*2 i*2-1];
        uv_out=evolution_sec(uhat(:,:,i+1),vhat(:,:,i+1),x,y,u(:,:,i+1),v(:,:,i+1),FpFu(:,:,i+1),FpFv(:,:,i+1),sec,dt,nu,mode1,order,display_onoff);
        uhat(:,:,i)=uv_out(:,:,1);
        vhat(:,:,i)=uv_out(:,:,2);
    end
end
uprime=zeros(num_y,num_x,2);
uprime(:,:,1)=uhat(:,:,1);
uprime(:,:,2)=vhat(:,:,1);
end

function out=evolution_sec(uhat,vhat,x,y,u,v,FpFu_s,FpFv_s,sec,dt,nu,mode1,order,display_onoff)
    num_x=length(x);
    num_y=length(y);
    %dx=x(2,1)-x(1,1);
    %dy=y(2,1)-y(1,1);
    rho=1;
    %uhat_c=zeros(num_y,num_x,2);
    %vhat_c=zeros(num_y,num_x,2);
    
    %starting from zero P
    P0=zeros(num_y,num_x);
    iter=100;
    for t=sec:-1:1
        RHSvar=zeros(iter,1);
        
        d2udx2=fd2udx2(uhat',x,mode1,order);
        d2udx2=d2udx2';
        d2vdx2=fd2udx2(vhat',x,mode1,order);
        d2vdx2=d2vdx2';
        d2udy2=fd2udx2(uhat,y,mode1,order);
        d2vdy2=fd2udx2(vhat,y,mode1,order);
        duhatdx=fdudx(uhat',x,mode1,order);
        duhatdx=duhatdx';
        dvhatdx=fdudx(vhat',x,mode1,order);
        dvhatdx=dvhatdx';
        duhatdy=fdudx(uhat,y,mode1,order);
        dvhatdy=fdudx(vhat,y,mode1,order);
        dudx=fdudx(u',x,mode1,order);
        dudx=dudx';
        dvdx=fdudx(v',x,mode1,order);
        dvdx=dvdx';
        dudy=fdudx(u,y,mode1,order);
        dvdy=fdudx(v,y,mode1,order);
        
        for k=1:1:iter
            Ps=P0;%p*
            dpdx=fdudx(Ps',x,mode1,order);
            dpdx=dpdx';
            dpdy=fdudx(Ps,y,mode1,order);
            
            uhats=uhat+dt/sec*(u.*duhatdx+v.*duhatdy-uhat.*dudx-vhat.*dvdx+dpdx+nu*d2udx2+nu*d2udy2-FpFu_s);%-dpdx
            vhats=vhat+dt/sec*(u.*dvhatdx+v.*dvhatdy-uhat.*dudy-vhat.*dvdy+dpdy+nu*d2vdx2+nu*d2vdy2-FpFv_s);%-dpdy
            %uhats(:,1)=zeros(num_y,1);
            %uhats(:,num_x)=zeros(num_y,1);
            %uhats(1,:)=zeros(1,num_x);
            %uhats(num_y,:)=zeros(1,num_x);
            %vhats(:,1)=zeros(num_y,1);
            %vhats(:,num_x)=zeros(num_y,1);
            %vhats(1,:)=zeros(1,num_x);
            %vhats(num_y,:)=zeros(1,num_x);
            
            duhatdx=fdudx(uhats',x,mode1,order);
            duhatdx=duhatdx';
            dvhatsdy=fdudx(vhats,y,mode1,order);
            RHS=-(duhatdx+dvhatsdy)/(dt/sec);
            pp=Poissoneqn(x,y,RHS,'z');
            P0=Ps+pp;
            RHSvar(k,1)=var(var(RHS));
        end
        uhat=uhats;
        vhat=vhats;
        P0=Ps;
        %figure(display_onoff(2))
        %plot(log(RHSvar))
        %hold on
    end
    %figure(display_onoff(2))
    %legend('sec 1','sec 2')
    %ylabel('log(Var(RHS))')
    %xlabel('iteration')
    out=zeros(num_y,num_x,2);
    out(:,:,1)=uhats/rho;
    out(:,:,2)=vhats/rho;
end