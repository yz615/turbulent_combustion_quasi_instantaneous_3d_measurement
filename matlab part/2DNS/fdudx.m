function y=fdudx(u,x,mode1,order)
%This code is to make the differentation of a certain variable.

N_u=size(u);
N_x=size(x);
if N_u(1)~=N_x(1)
    disp('Inequality between the u and x in function fdudx')
else
    N=N_x(1);
    dudx=zeros(N_u(1),N_u(2));
    if mode1==0
        switch order
            case 4
                for i=3:1:N-2
                    dudx(i,:)=(8*u(i+1,:)-8*u(i-1,:)-u(i+2,:)+u(i-2,:))/6/(x(i+1,:)-x(i-1,:));%4 order
                end
                dudx(1,:)=(-25*u(1,:)+48*u(2,:)-36*u(3,:)+16*u(4,:)-3*u(5,:))/12/(x(2,:)-x(1,:));%4 order
                dudx(2,:)=(18*u(3,:)-10*u(2,:)-3*u(1,:)-6*u(4,:)+u(5,:))/12/(x(2,:)-x(1,:));%4 order
                dudx(N,:)=(25*u(N,:)-48*u(N-1,:)+36*u(N-2,:)-16*u(N-3,:)+3*u(N-4,:))/12/(x(N,:)-x(N-1,:));%4 order
                dudx(N-1,:)=(18*u(N-2,:)-10*u(N-1,:)-3*u(N,:)-6*u(N-3,:)+u(N-4,:))/12/(x(N-1,:)-x(N,:));%4 order
            case 2
                for i=2:1:N-1
                    dudx(i,:)=(u(i+1,:)-u(i-1,:))/(x(i+1,:)-x(i-1,:));%2 order
                end
                dudx(1,:)=(-3*u(1,:)+4*u(2,:)-u(3,:))/2/(x(2,:)-x(1,:));%2 order
                dudx(N,:)=(3*u(N,:)-4*u(N-1,:)+u(N-2,:))/2/(x(N,:)-x(N-1,:));%2 order
        end
    end
end
y=dudx;