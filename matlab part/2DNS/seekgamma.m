function gamma_out=seekgamma(x,y,u_gd,v_gd,pepu,pepv,um,vm,dt,F,nu,num_t,mode1,order,sec)

%start=0;
%start=0;
e0=calcJ(u_gd,v_gd,x,y,um,vm,F,dt,nu,num_t,mode1,order,sec);
N=10;
gamma=zeros(N,1);
e=2*e0*ones(N,1);
gamma_min=1;% the number of minimal gamma
e_min=e0;
e(1)=e0;
gap0=0.01;
%start to dientify the direction
ep=calcJ(u_gd-gap0*pepu,v_gd-gap0*pepv,x,y,um,vm,F,dt,nu,num_t,mode1,order,sec);
en=calcJ(u_gd+gap0*pepu,v_gd+gap0*pepv,x,y,um,vm,F,dt,nu,num_t,mode1,order,sec);

if (~isnan(ep))&&(~isnan(en))
    if ep<e_min
        gamma(2)=gap0;
        e(2)=ep;
        e_min=ep;
        gamma_min=2;
        %positive
    else
        if en<e_min
            gamma(2)=-gap0;
            e(2)=en;
            e_min=en;
            gamma_min=2;
            %negative
        else
            disp('Something wrong with gamma direction!')
        end
    end
end

%start to propagate
for i=3:1:N
    if gamma(i)==0
        gamma(i)=gamma(i-1)*2;
    end
    e(i)=calcJ(u_gd-gamma(i)*pepu,v_gd-gamma(i)*pepv,x,y,um,vm,F,dt,nu,num_t,mode1,order,sec);
    if isnan(e(i))
        gamma(i)=gamma(i-1)*1.5;
        e(i)=calcJ(u_gd-gamma(i)*pepu,v_gd-gamma(i)*pepv,x,y,um,vm,F,dt,nu,num_t,mode1,order,sec);
        if isnan(e(i))
            break
        else
            if e(i)>e0
                break
            else
                if e(i)<e_min
                    e_min=e(i);
                    gamma_min=i;
                end
            end
        end
    else
        if e(i)>e0
            break
        else
            if e(i)<e_min
                e_min=e(i);
                gamma_min=i;
            end
        end
    end
end

gamma_minvalue=gamma(gamma_min);

vareps=0.001;
N=100;
gamma=zeros(N,1);
gamma(1)=gamma_minvalue;
e=zeros(N,1);
e(1)=e_min;
pepg=1;
for j=1:1:N-1
    if j>1
        e(j)=calcJ(u_gd-gamma(j)*pepu,v_gd-gamma(j)*pepv,x,y,um,vm,F,dt,nu,num_t,mode1,order,sec);
    end
    if isnan(e(j))
        j=j-1;
        break
    end
    pepg=(calcJ(u_gd-(gamma(j)+pepg*vareps)*pepu,v_gd-(gamma(j)+pepg*vareps)*pepv,x,y,um,vm,F,dt,nu,num_t,mode1,order,sec)-e(j))/(pepg*vareps);
    gamma(j+1)=gamma(j)-vareps*pepg;
    if (abs(gamma(j)-gamma(j))<10^-4)&&(j>3)
        break;
    end
    if calcJ(u_gd-gamma(j+1)*pepu,v_gd-gamma(j+1)*pepv,x,y,um,vm,F,dt,nu,num_t,mode1,order,sec)>e(j)
        vareps=0.001;
    end
end
gamma_out=[gamma(j) e(j)];
%gamma(find(gamma==0))=[];
%disp(gamma)
%disp('***')