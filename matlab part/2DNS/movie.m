% making movie
for j=0:1:9
    figtitle=['step ' num2str(j+1) ' velocity comparison.fig'];
    open(figtitle)
    set(gca,'Fontsize',16,'Fontname','Times')
    %caxis([0,0.2]);
    frame=getframe(gcf);
    im=frame2im(frame); 
    [I,map]=rgb2ind(im,20);          

    if j==0
        imwrite(I,map,'2Derror.gif','gif', 'Loopcount',inf,'DelayTime',1);
    elseif j==9
        imwrite(I,map,'2Derror.gif','gif','WriteMode','append','DelayTime',1);
    else
        imwrite(I,map,'2Derror.gif','gif','WriteMode','append','DelayTime',1);
    end
end