function out=Poissoneqn(x,y,RHS,mode)
[X,Y]=meshgrid(x,y);
fun=@(location,state)interp2(X,Y,RHS,location.x,location.y);

model = createpde();
SQ1 = [3,4,0,1,1,0,1,1,0,0]';
g=decsg(SQ1);
geometryFromEdges(model,g);
%pdegplot(model,'EdgeLabels','on')
applyBoundaryCondition(model,'dirichlet','Edge',1:model.Geometry.NumEdges,'u',0);
specifyCoefficients(model,'m',0,'d',0,'c',-1,'a',0, 'f',fun); %minus form of Poisson equation
generateMesh(model,'Hmax',0.05);
results = solvepde(model);
%pdeplot(model,'XYData',results.NodalSolution)
querypoints = [X(:),Y(:)]';
pintrp=interpolateSolution(results,querypoints);
out = reshape(pintrp,size(X));