function u_out=reinterp1u(u,x,F)
%this function is to smooth the velocity distribution
f_F=sum(F);
f_F=f_F';
x_F=f_F.*x;
u_F=f_F.*u;
u_F(all(u_F==0,2),:)=[];
x_F(all(x_F==0,2),:)=[];
u_out=zeros(length(x),1);
for i=1:1:length(x)
    u_out(i)=interp1(x_F,u_F,x(i),'spline');
end