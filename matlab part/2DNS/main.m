clc
clear
%main

%reference variable
L0=2*pi;%length of the field in scanning direction (x direction)
L_y=2*pi;%length of the field in the y direction
t0=1;%1 s,total scanning time
num_y=23;% number of the guessing points=time points (Nx+1)
num_x=17;%number of the point in y direction(Ny+1);
num_t=num_x+1;
u_s=L0/t0;
%nu=1.5*10^-5;
nu=0;
nu=nu/L0/u_s;
dt=t0/(num_t-1-1);%This is because what we used as the initial velocity is not the 0 step.

%initial condition for the test model
x=0:1/(num_x-1):1;
x=x';
y=(0:1/(num_y-1):1)*L_y/L0;
y=y';

u0=zeros(num_y,num_x);
v0=zeros(num_y,num_x);

%initialization of u0 and v0
x_mid=(num_x+1)/2;
y_mid=(num_y+1)/2;
V0=zeros(num_y,num_x);
%test velocity field 1: u0=0.6*sin(pi*x(i))*sin(pi*y(j)); v0(j,i)=0.6*cos(pi*x(i))*cos(pi*y(j))
%test velocity field 2: u0=V0(i,j)*(0.5-y(j))/max(d,0.001);v0(j,i)=V0(i,j)*(x(i)-0.5)/max(d,0.001);
for i=1:1:num_x
    for j=1:1:num_y
        %d=((x(i)-0.5)^2+(y(j)-0.5)^2)^0.5;
        %V0(i,j)=2*d*exp(-8*d);%x(i)*y(j)*(1-x(i))*(L_y/L0-y(j));%non-dimensional
        u0(j,i)=0.6*(sin(pi*x(i)))*sin(pi*y(j));
        v0(j,i)=0;
    end
end
[X,Y]=meshgrid(x,y);
figure(1)
quiver(X,Y,u0,v0,'linewidth',1.25)
xlabel('x/L_0')
ylabel('y/L_0')

mode1=0;
order=2;
sec=2;
display_onoff=[0 2];

%reorder the input of u and v to single vector
input_uv=[u0(:);v0(:)];

out_0=evolution_2Din(x,y,input_uv,nu,num_t,dt,display_onoff,mode1,order,sec);
u=out_0(:,:,:,1);
v=out_0(:,:,:,2);
maxV=zeros(num_t,1);
for i=2:1:num_t
    maxV(i)=max(max(sqrt(u(:,:,i).^2+v(:,:,i).^2)));
end
for j=3+1:1:3+num_t
    figure(j)
    contour(X,Y,sqrt(u(:,:,j-3).^2+v(:,:,j-3).^2),'linewidth',1)
    hold on
    quiver(X,Y,u(:,:,j-3),v(:,:,j-3),'linewidth',1.25)
    hold on
    title(['Time step ' num2str(j-5) ' t/t_0=' num2str((j-5)*dt)])
    xlabel('X')
    ylabel('Y')
    set(gca,'Fontsize',16,'Fontname','Times')
    figtitle=['DNS_step_' num2str(j-4)];
end
figure(4+num_t)
for i=1:1:num_t
    plot(x,u(round(num_y/2,0),:,i));
    hold on
end

%measurement
um=zeros(num_y,num_x);
vm=zeros(num_y,num_x);
for j=1:1:num_x
    um(:,j)=u(:,j,j+1);
    vm(:,j)=v(:,j,j+1);
end
figure(5+num_t)
quiver(X,Y,u(:,:,2),v(:,:,2),'linewidth',1.25)
hold on
quiver(X,Y,um,vm,'linewidth',1.25)
legend('initial','measurement')
xlabel('X')
ylabel('Y')
set(gca,'Fontsize',16,'Fontname','Times')
%xlswrite('data.xlsx',X,'Sheet 1')
%xlswrite('data.xlsx',Y,'Sheet 2')
%xlswrite('data.xlsx',u(:,:,2),'Sheet 3')
%xlswrite('data.xlsx',v(:,:,2),'Sheet 4')
%xlswrite('data.xlsx',um,'Sheet 5')
%xlswrite('data.xlsx',vm,'Sheet 6')