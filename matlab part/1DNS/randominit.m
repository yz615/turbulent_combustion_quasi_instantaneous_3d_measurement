%random initialization

%this file will generate a group of initial data and evaluate the variance
%matrix of these data.

L0=2*pi;%length of the whole field
t0=0.95;%1 s,total scanning time
Nt=30;% number of the guessing points=time points
u_s=L0/t0;
%nu_0=1.5*10^-5;
nu_0=0.1;
nu=nu_0/L0/u_s;
dt=t0/(Nt-1);
Nx=Nt+2;

%initial condition for the test model
x=(0:1/(Nx-1):1)';
u0=1.7*cos(L0*x/2)/u_s;%/u_s;%non dimension

%define Boundary condition
alpha_1=0.5/u_s*sin(0:2*pi/(Nt-1):2*pi)+1.5/u_s*ones(1,Nt);
alpha_Nx=-1.5/u_s*ones(1,Nt)+0.6/u_s*sin(0:2*pi/(Nt-1):2*pi);
BC=[alpha_1;alpha_Nx];
BC=BC(:);

%define input as gamma
gamma=[u0(2:end-1);BC];

u0_group=u0*normrnd(1,0.05,[1,200])+normrnd(0,0.001,[Nx,200]);
