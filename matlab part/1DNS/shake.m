function pepu_i=shake(u_start,du_start,x,u_m,F,dt,nu,num,e_start)
%this function is to calculate the differentitation of the function 
%using shake the box

e=-10:0.1:10;
e(find(e==0))=[];

pepu=zeros(length(e),1);
for i=1:1:length(e)
    pepu(i)=(calcJ(u_start+du_start*e(i),x,u_m,F,dt,nu,num)-e_start)/e(i);
end

plot(e,pepu)
pepu_i=pepu(101);