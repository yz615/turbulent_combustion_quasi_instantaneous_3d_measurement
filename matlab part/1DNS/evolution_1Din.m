function y=evolution_1Din(x,gamma,nu,Nt,dt,display_onoff,mode1,order,sec,BCoff)
% this function is to calculate the evolution of 1D incompressible flow
% nu is the viscosity
% num is the number of time iteration,num does not consider the initial 0
% dt is the time step
% sec is the number of section between each time step; otherwise, it won't
% converge on time.
Nx=length(x);
y=zeros(Nx,Nt);

if BCoff~=0
    u0=gamma(1:Nx-2,1);
    BC=reshape(gamma(Nx-1:end,1),2,Nt);
    y(:,1)=[BC(1,1);u0;BC(2,1)];
else
    u0=gamma;
    Nu=length(u0);%N*1
    if Nu~=Nx-2
        disp('Inequality between the u and x in function evolution_1Din')
    end
    %y(:,1)=[u0(2)*2-u0(1);u0;u0(end-1)*-u0(end)];
    y(:,1)=u0;
end

for i=2:1:Nt
    %start the time propagation
    if BCoff==1
        y(:,i)=evolution_sec(y(:,i-1),x,sec,dt,nu,mode1,order,BC(:,i-1:i));
    else
        y(:,i)=evolution_sec(y(:,i-1),x,sec,dt,nu,mode1,order,0);
    end
end


if display_onoff~=0
    figure(display_onoff)
    for i=1:1:Nt
        plot(x,y(:,i));
        hold on
    end
end
end

function out=evolution_sec(u_p,x,sec,dt,nu,mode1,order,BC)
BCs=length(BC);
if BCs==1
    if BC==0
        for k=1:1:sec
            %If we don't have BC as the input in the evolution, just cut
            %the BC off, which mean we only have (2:end-1) for u and x
            dudx_sec=fdudx(u_p(2:end-1),x(2:end-1),mode1,order);
            d2udx2_sec=fd2udx2(u_p(2:end-1),x(2:end-1),mode1,order);
            u_predict=u_p(2:end-1)+dt/sec*(-u_p(2:end-1).*dudx_sec+nu*d2udx2_sec);
            dudx_pre=fdudx(u_predict,x(2:end-1),mode1,order);
            d2udx2_pre=fd2udx2(u_predict,x(2:end-1),mode1,order);
            u_p(2:end-1)=(u_p(2:end-1)+u_predict)/2+dt/sec/2*(-u_predict.*dudx_pre+nu*d2udx2_pre);
        end
    else
        disp('Wrong BC!')
    end
else
    for k=1:1:sec
        u_p(1)=BC(1,1)+(k-1)/sec*(BC(1,2)-BC(1,1));
        u_p(length(x))=BC(2,1)+(k-1)/sec*(BC(2,2)-BC(2,1));
        dudx_sec=fdudx(u_p,x,mode1,order);
        d2udx2_sec=fd2udx2(u_p,x,mode1,order);
        u_predict=u_p+dt/sec*(-u_p.*dudx_sec+nu*d2udx2_sec);
        u_predict(1)=BC(1,1)+k/sec*(BC(1,2)-BC(1,1));
        u_predict(length(x))=BC(2,1)+k/sec*(BC(2,2)-BC(2,1));
        dudx_pre=fdudx(u_predict,x,mode1,order);
        d2udx2_pre=fd2udx2(u_predict,x,mode1,order);
        u_p=(u_p+u_predict)/2+dt/sec/2*(-u_predict.*dudx_pre+nu*d2udx2_pre);
        %u_p(1)=BC(1,2);
        %u_p(length(x))=BC(2,2);
    end
end
out=u_p;    
end