function uprime=evolution_1Din_adjoint(x,y_start,nu,Nt,dt,F,u_m,mode1,order)
%this function is to calculate the evolution of adjoint variable back side

N_u=length(y_start);%N*num
N_x=length(x);
e=diag(F*y_start)-u_m;
if N_u~=N_x
    disp('Inequality between the u and x in function evolution_1Din')
else
    N=N_u;
    uprime=zeros(N,Nt);
    for i=Nt:-1:2
        %previous term
        u_prime(1)=0;
        u_prime(N)=0;
        u_p=uprime(:,i);
        dupdx=fdudx(u_p,x,mode1,order);
        d2updx2=fd2udx2(u_p,x,mode1,order);
        f_e=F(i,:);
        f_e=f_e';
        u_predict=u_p+dt*(y_start(:,i).*dupdx+nu*d2updx2-f_e*e(i));
        u_predict(1)=0;
        u_predict(N)=0;
        dudx_pre=fdudx(u_predict,x,mode1,order);
        d2udx2_pre=fd2udx2(u_predict,x,mode1,order);
        f_e=F(i-1,:);
        f_e=f_e';
        uprime(:,i-1)=(u_predict+u_p)/2+dt/2*(y_start(:,i-1).*dudx_pre+nu*d2udx2_pre-f_e*e(i-1));
        %uprime(1,i-1)=0;
        %uprime(N,i-1)=0;
    end
end
