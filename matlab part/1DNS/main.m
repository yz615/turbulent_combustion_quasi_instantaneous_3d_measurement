clc
clear
%main

%reference variable
L0=2*pi;%length of the whole field
t0=0.95;%1 s,total scanning time
Nt=30;% number of the guessing points=time points
u_s=L0/t0;
%nu_0=1.5*10^-5;
nu_0=0.1;
nu=nu_0/L0/u_s;
dt=t0/(Nt-1);
Nx=Nt+2;

%initial condition for the test model
x=(0:1/(Nx-1):1)';
u0=1.7*cos(L0*x/2)/u_s;%/u_s;%non dimension

%define Boundary condition
alpha_1=0.5/u_s*sin(0:2*pi/(Nt-1):2*pi)+1.5/u_s*ones(1,Nt);
alpha_Nx=-1.5/u_s*ones(1,Nt)+0.6/u_s*sin(0:2*pi/(Nt-1):2*pi);
BC=[alpha_1;alpha_Nx];
BC=BC(:);

%define input as gamma
gamma=[u0(2:end-1);BC];

%corvariance matrix initialization
u0_group=u0*normrnd(1,0.05,[1,200])+normrnd(0,0.001,[Nx,200]);
u0_e=mean(u0_group(2:end-1,:)')';
B_u0=cov(u0_group(2:end-1,:)');
alpha0_e=mean(u0_group(1,:));
alphaNx_e=mean(u0_group(end,:));
alpha_e=[alpha0_e;alphaNx_e]*ones(1,Nt);
gamma_e=[u0_e;alpha_e(:)];
B=[B_u0 zeros(Nt,Nt*2);zeros(Nt*2,Nt) ones(Nt*2,Nt*2)];

%essential factors
mode1=0; %central differentiation 
order=2; %2nd order differentiation
sec=4;   %steps in the Runge-Kutta method
BCoff=1; %0 for not optimize BC; 1 for optimize BC 



%baseline model
display_onoff=0;
y=evolution_1Din(x,gamma,nu,Nt,dt,display_onoff,mode1,order,sec,BCoff);

%measurement operator
vstep=1;%vstep=1 means each node in this calculation has been measured and their is not extra point analysed.
F=zeros(length(x),Nt);
for i=1:1:Nt
    F(i*vstep+1,i)=1;
end
F=F';
u_m=diag(F*y);

%run measurement
dx=x(2)-x(1);
v_scan=dx/dt*vstep;
t_m=(0:dt:dt*(Nt-1))';
x_m=v_scan*t_m+dx;
x_m=[x(1);x_m;x(end)];


%starting adjoint method
%give a guess
num_m=length(x_m);
%u_start=u0(2:end-1);
u_start=u_m;
%u_start=zeros(Nx-2,1);


%alpha_start=BC;
alpha_start=[u_start(1)*ones(1,Nt);u_start(end)*ones(1,Nt)];
%alpha_start=alpha_out;
gamma_start=[u_start;alpha_start(:)];
BCoff=1;
[J0,grad0]=calcJ(gamma_start,x,u_m,F,B,gamma_e,dt,nu,Nt,mode1,order,sec,BCoff,1);
%test calaJ
disp('J_0=')
disp(J0)


%test pepu of u0
pepu_u0=zeros(length(u0),1);
for i=2:1:Nx-1
    dgamma=zeros(length(gamma_start),1);
    delta=-0.001;
    dgamma(i-1)=delta;
    pepu_u0(i)=(calcJ(gamma_start+dgamma,x,u_m,F,B,gamma_e,dt,nu,Nt,mode1,order,sec,BCoff,1)-J0)/delta;
end

figure(2)
grad_u0=grad0(1:Nx-2,1);
plot(x(2:end-1),grad_u0,'linewidth',2)
hold on 
plot(x(2:end-1),pepu_u0(2:end-1),'linewidth',2)
legend({'grad($u_0$) from adjoint','grad($u_0$) from shake the box'},'interpreter','latex')
xlabel('$x/x_0$','interpreter','Latex')
ylabel('gradient of $\mathcal{L},\frac{\partial \mathcal{L}}{\partial \alpha}$','interpreter','latex')
set(gca,'Fontsize',16,'Fontname','Times')

%test pepu of alpha_N
pepu_alphaN=zeros(Nt,1);
for i=1:1:Nt
    dgamma=zeros(length(gamma_start),1);
    delta=-0.001;
    dgamma(Nx-2+2*i)=delta;
    pepu_alphaN(i)=(calcJ(gamma_start+dgamma,x,u_m,F,B,gamma_e,dt,nu,Nt,mode1,order,sec,BCoff,1)-J0)/delta;
end

figure(3)
grad_alpha=reshape(grad0(Nx-1:end,1),2,Nt);
plot(0:Nt-1,grad_alpha(2,:),'linewidth',2);
hold on
plot(0:Nt-1,pepu_alphaN,'linewidth',2);
legend({'$\alpha_N$ from adjoint','$\alpha_N$ from shake the box'},'interpreter','latex')
xlabel({'time step $(t/\Delta t)$'},'interpreter','Latex')
ylabel('gradient of $\mathcal{L},\frac{\partial \mathcal{L}}{\partial \alpha}$','interpreter','latex')
set(gca,'Fontsize',16,'Fontname','Times')

%  using fminunc
%  Set options for fminunc
%options = optimset('GradObj', 'on', 'MaxIter', 20);
options = optimoptions(@fminunc,'Display','iter','Algorithm','quasi-newton');
[gamma_out,cost,exitflag,output]=fminunc(@(gamma)(calcJ(gamma,x,u_m,F,B,gamma_e,dt,nu,Nt,mode1,order,sec,BCoff,1/J0)),gamma_start,options);
%for i=1:1:5
%    gamma_start=[gamma_out;gamma_start(Nx-1:end)];
%    [gamma_out,cost,exitflag,output]=fminunc(@(gamma)(calcJ(gamma,x,u_m,F,B,gamma_e,dt,nu,Nt,mode1,order,sec,BCoff,1/J0)),gamma_start,options);
%end

figure(4)
plot(x,u0,'linewidth',2)
hold on
scatter(x_m(2:end-1,1),u_m)
plot(x(2:end-1),u_start,'linewidth',2)
xlabel({'$x/L_0$'},'Interpreter','Latex')
ylabel({'$u/u_0$'},'Interpreter','Latex')
figtitle=['Guess at step 0'];
legend('Initial velocity from DNS result','Measurement points','Prediction')
title(figtitle)
set(gca,'Fontsize',16,'Fontname','Times')
axis([0 1 -0.5 0.5])

figure(5)
plot(x,u0,'linewidth',2)
hold on
scatter(x_m(2:end-1,1),u_m)
plot(x(2:end-1),gamma_out(1:Nx-2),'linewidth',2)
grid on
xlabel({'$x/L_0$'},'interpreter','Latex')
ylabel({'$u/u_0$'},'interpreter','Latex')
figtitle=['Final results'];
legend('Initial velocity from DNS result','Measurement points','Prediction')
title(figtitle)
set(gca,'Fontsize',16,'Fontname','Times')
axis([0 1 -0.5 0.5])

figure(6)
plot(0:Nt-1,alpha_Nx,'linewidth',2);
hold on
alpha_out=reshape(gamma_out(Nx-1:end),2,Nt);
plot(0:Nt-1,alpha_out(2,:),'linewidth',2)
xlabel({'time step $(t/\Delta t)$'},'interpreter','Latex')
ylabel({'$\alpha_N/u_s$ '},'interpreter','Latex')
legend({'Given $\alpha_N$','Predicted $\alpha_N$'},'interpreter','Latex')
set(gca,'Fontsize',16,'Fontname','Times')
axis([0 30 -0.35 0])



