function [J,grad]=calcJ(gamma,x_g,u_m,F,B,gamma_e,dt,nu,Nt,mode1,order,num_sec,BCoff,f_norm)
%this function is to calculate the error between guess and measurement
%gamma: guess of the velocity
%f_norm: normalization factor, to enlarge the initial J to 1; otherwise it
%will be too small
display_onoff=0;
dx=x_g(2)-x_g(1);
y_g=evolution_1Din(x_g,gamma,nu,Nt,dt,display_onoff,mode1,order,num_sec,BCoff);
u_pred_temp=diag(F*y_g);
u_pred=u_pred_temp(1:length(u_m));
N=length(u_m);
f_int=ones(N,1);
f_int(1)=1/2;
f_int(N)=1/2;
J=1/2*sum(f_int.*(u_pred-u_m).*(u_pred-u_m))*dx*dt;% times dx and dt is because it is the space integration and time integration

%add variance information into J
J=J+1/2*(gamma-gamma_e)'*(gamma-gamma_e)*dx*dt;

%normalization
J=J*f_norm;

%y_start=evolution_1Din(x_g,u_g,nu,num,dt,0,mode1,order,sec,0);
uprime=evolution_1Din_adjoint(x_g,y_g,nu,Nt,dt,F,u_m,mode1,order);
dupdx=fdudx(uprime,x_g,mode1,order);
ddupddx=fd2udx2(uprime,x_g,mode1,order);
if BCoff==0
    %not considering the boundary condition
    grad=-uprime(2:end-1,1)*dx;
else
    %considering the boundary condition
    
    %gradient of the initial velocity
    grad_u0=-uprime(2:end-1,1)*dx;
    %gradient of the boundary conditions
    %no viscous term
    grad_alpha_00=-1/2*dx*uprime(1,1)+dt*uprime(1,1)*y_g(1,1)+1/2*dt*1/2*dx*(-y_g(1,1)*dupdx(1,1)-(uprime(1,2)-uprime(1,1))/dt);
    grad_alpha_10=-1/2*dx*uprime(end,1)-dt*uprime(end,1)*y_g(end,1)+1/2*dt*1/2*dx*(-y_g(end,1)*dupdx(end,1)-(uprime(end,2)-uprime(end,1))/dt);
    grad_alpha_00=grad_alpha_00/2;
    grad_alpha_10=grad_alpha_10/2;%no idea why there is an extra 1/2 here, but it works
    grad_alpha_0t=1/2*dx*dt*(-y_g(1,2:end-1).*dupdx(1,2:end-1)-(uprime(1,3:end)-uprime(1,1:end-2))/2/dt);
    grad_alpha_1t=1/2*dx*dt*(-y_g(end,2:end-1).*dupdx(end,2:end-1)-(uprime(end,3:end)-uprime(end,1:end-2))/2/dt);
    %end terms are zero
    %grad_alpha_01=-1/2*1/2*dx*dt*(-y_g(1,end).*dupdx(1,end)-(uprime(1,end)-uprime(1,end-1))/dt);
    %grad_alpha_11=-1/2*1/2*dx*dt*(-y_g(end,end).*dupdx(end,end)-(uprime(end,end)-uprime(end,end-1))/dt);
    %combination
    grad_alpha=[grad_alpha_00 grad_alpha_0t 0;grad_alpha_10 grad_alpha_1t 0;];
    %viscous term
    grad_alpha(:,1)=grad_alpha(:,1)-nu*([ddupddx(1,1);ddupddx(end,1)]*dt*dx*1/2*1/2*1/2);
    grad_alpha(:,1)=grad_alpha(:,1)+nu*([-dupdx(1,1);dupdx(end,1)*dt]*1/2);
    grad_alpha(:,2:end-1)=grad_alpha(:,2:end-1)-nu*([ddupddx(1,2:end-1);ddupddx(end,2:end-1)]*dt*dx*1/2);
    grad_alpha(:,2:end-1)=grad_alpha(:,2:end-1)+nu*([-dupdx(1,2:end-1);dupdx(end,2:end-1)*dt]);
    %end terms are zero
    %grad_alpha(:,end)=grad_alpha(:,end)-nu*([ddupddx(1,end);ddupddx(end,end)]*dt*dx*1/2*1/2*1/2);
    %grad_alpha(:,end)=grad_alpha(:,end)+nu*([dupdx(1,end);dupdx(end,end)*dx]*1/2);
    
    grad=[grad_u0;grad_alpha(:);];
    
    %add gradient coming from variance information
    grad=grad+(gamma-gamma_e)*dx*dt;
    
    grad=grad*f_norm;
end

